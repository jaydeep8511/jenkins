package testCases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import pages.AddNumber;
import pages.LoginPage;
import pages.RegistrationPage;
import pages.USAllocateNumbers;
import pages.USUser;
import pages.USVoicemail;
import pages.UsersPage;
import utilsFile.PropertiesFile;
import utilsFile.Retry;
import utilsFile.Utilitylib;

public class USAllocateNumbersTest {

	USAllocateNumbers usAllocateNumbers;
	USUser usUser;
	UsersPage userPage;
	AddNumber addNumber;
	LoginPage loginPage;
	WebDriver driver;
	static Utilitylib excel;
	RegistrationPage registrationPage;
	PropertiesFile url;

	public USAllocateNumbersTest() throws Exception {
		url = new PropertiesFile();
		excel = new Utilitylib("..\\Web\\src\\main\\java\\config\\Signup.xlsx");
	}

	@BeforeMethod
	public void initialization() throws IOException {

		driver = TestBase.init();

		usAllocateNumbers = PageFactory.initElements(driver, USAllocateNumbers.class);
		usUser = PageFactory.initElements(driver, USUser.class);
		userPage = PageFactory.initElements(driver, UsersPage.class);
		addNumber = PageFactory.initElements(driver, AddNumber.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = result.getTestContext().getName()+" - Fail";
			Utilitylib.Screenshot(driver, testname,"Fail "+result.getMethod().getMethodName());
		} else {
			String testname = result.getTestContext().getName()+" - Pass";
			Utilitylib.Screenshot(driver, testname,"Pass "+result.getMethod().getMethodName());
		}
		driver.quit();
	}

	public String signupAndSignin() throws Exception {
		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		driver.get(url.signUp());
		registrationPage.enterFullname("tstAutomation");
		registrationPage.enterCompanyName("appitSimple");
		registrationPage.enterMobile("918511695975");
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("12345678");
		registrationPage.clickOnSignupButton();
		boolean actualResultr = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResultr = true;
		assertEquals(actualResultr, expectedResultr);

		registrationPage.ValidateVerifyButtonOnGmail();
//		boolean actualVerifyMsg = registrationPage.accountVerifiedMsg();
//		boolean expectedVerifyMsg = true;
//		assertEquals(actualVerifyMsg, expectedVerifyMsg);
//
//		driver.get(url.signIn());
//		loginPage.enterEmail(email);
//		loginPage.enterPassword("12345678");
//		loginPage.clickOnLogin();

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.closePopup();
		addNumber.closePopup();

		return email;
	}
	
	public String addNumberAndOpenTheirNumberSettingPage() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();

//		addNumber.selectSearchByTollfree();
//		addNumber.clickOnSearchButton();

		String numberBeforePurchased = addNumber.getSeletedNumber();
		addNumber.selectNumber();
		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();
		addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$8.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		String NumberAfterPurchased = addNumber.getNumeberAfterPurchased();
		assertEquals(numberBeforePurchased, NumberAfterPurchased);

		return NumberAfterPurchased;
		// numberSettingPage.invisiblityOfValidationMessage();

	}
	
	public String add_second_number() {
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 2");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber2();

		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();

		addNumber.waitForAddNumberPage();

		addNumber.clickOnSaveButton();

		String actualMessage1 = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage1 = "Number added successfully";
		assertEquals(actualMessage1, expectedMessage1);
		
		return addNumber.getSecondNumeberAfterPurchased();
	}
	
	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_deAllocate_number_when_only_one_number_is_assigned_User() throws Exception {
		String email = signupAndSignin();
		
		String number= addNumberAndOpenTheirNumberSettingPage();
		
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAllocateNumbers.clickOnNumber(number);
		usAllocateNumbers.waitForAllocateErrorMsg();
		String actualValidationMsg = usAllocateNumbers.validationMessage();
		String expectedValidationMsg = "A number should be allocated to atleast one user.";
		assertEquals(actualValidationMsg, expectedValidationMsg);
		
	}
	
	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_Unchecked_set_as_default_number_checkbox_while_only_one_number_is_assigned_User() throws Exception {
		String email = signupAndSignin();
		
		String number= addNumberAndOpenTheirNumberSettingPage();
		
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAllocateNumbers.clickOnDefaultNumberCheckBoxOf(number);
		usAllocateNumbers.waitForAllocateErrorMsg();
		String actualValidationMsg1 = usAllocateNumbers.validationMessage();
		String expectedValidationMsg1 = "A number should be allocated to atleast one user.";
		assertEquals(actualValidationMsg1, expectedValidationMsg1);	
		
	}
	
	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_deallocate_second_Number() throws Exception {
		String email = signupAndSignin();
		
		String number= addNumberAndOpenTheirNumberSettingPage();
		String secondNumber = add_second_number();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
	
		usAllocateNumbers.clickOnNumber(secondNumber);
		usAllocateNumbers.clickOnNoButton();
		usAllocateNumbers.clickOnNumber(secondNumber);
		usAllocateNumbers.clickOnYesButton();
		String actualValidationMsg1 = usAllocateNumbers.validationMessage();
		String expectedValidationMsg1 = "Number deallocated successfully";
		assertEquals(actualValidationMsg1, expectedValidationMsg1);	
		
	}
	
	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_allocate_second_Number() throws Exception {
		String email = signupAndSignin();
		
		String number= addNumberAndOpenTheirNumberSettingPage();
		String secondNumber = add_second_number();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
	
		usAllocateNumbers.clickOnNumber(secondNumber);
		usAllocateNumbers.clickOnNoButton();
		usAllocateNumbers.clickOnNumber(secondNumber);
		usAllocateNumbers.clickOnYesButton();
		String actualValidationMsg1 = usAllocateNumbers.validationMessage();
		String expectedValidationMsg1 = "Number deallocated successfully";
		assertEquals(actualValidationMsg1, expectedValidationMsg1);
		usAllocateNumbers.closeValidationMessage();
		
		usAllocateNumbers.clickOnNumber(secondNumber);
		usAllocateNumbers.clickOnYesButton();
		String actualValidationMsg = usAllocateNumbers.validationMessage();
		String expectedValidationMsg = "Number allocated successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
		
	}
	
	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_deallocate_Number_which_checked_By_default_number_checkbox() throws Exception {
		String email = signupAndSignin();
		
		String number= addNumberAndOpenTheirNumberSettingPage();
		String secondNumber = add_second_number();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
	
		usAllocateNumbers.clickOnNumber(number);
		String actualValidationMsg1 = usAllocateNumbers.validationMessage();
		String expectedValidationMsg1 = "First set another default number.";
		assertEquals(actualValidationMsg1, expectedValidationMsg1);	
		
	}
	
	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_checked_By_default_number_checkbox_of_Second_Number() throws Exception {
		String email = signupAndSignin();
		
		String number= addNumberAndOpenTheirNumberSettingPage();
		String secondNumber = add_second_number();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
	
		usAllocateNumbers.clickOnDefaultNumberCheckBoxOf(secondNumber);
		usAllocateNumbers.clickOnNoButton();
		usAllocateNumbers.clickOnDefaultNumberCheckBoxOf(secondNumber);
		usAllocateNumbers.clickOnYesButton();
		String actualValidationMsg1 = usAllocateNumbers.validationMessage();
		String expectedValidationMsg1 = "Default number changes successfully";
		assertEquals(actualValidationMsg1, expectedValidationMsg1);	
		
	}
	
	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_checked_By_default_number_checkbox_of_First_Number() throws Exception {
		String email = signupAndSignin();
		
		String number= addNumberAndOpenTheirNumberSettingPage();
		String secondNumber = add_second_number();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
	
		
		usAllocateNumbers.clickOnDefaultNumberCheckBoxOf(secondNumber);
		usAllocateNumbers.clickOnYesButton();
		String actualValidationMsg1 = usAllocateNumbers.validationMessage();
		String expectedValidationMsg1 = "Default number changes successfully";
		assertEquals(actualValidationMsg1, expectedValidationMsg1);	
		
		usAllocateNumbers.clickOnDefaultNumberCheckBoxOf(number);
		usAllocateNumbers.clickOnYesButton();
		String actualValidationMsg2 = usAllocateNumbers.validationMessage();
		String expectedValidationMsg2 = "Default number changes successfully";
		assertEquals(actualValidationMsg2, expectedValidationMsg2);	
		
	}
	
	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_By_default_number_checkbox_not_displayed_when_number_is_unassigned() throws Exception {
		String email = signupAndSignin();
		
		String number= addNumberAndOpenTheirNumberSettingPage();
		String secondNumber = add_second_number();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
	
		usAllocateNumbers.clickOnNumber(secondNumber);
		usAllocateNumbers.clickOnYesButton();
		String actualValidationMsg1 = usAllocateNumbers.validationMessage();
		String expectedValidationMsg1 = "Number deallocated successfully";
		assertEquals(actualValidationMsg1, expectedValidationMsg1);
		usAllocateNumbers.closeValidationMessage();
		
		boolean actualResult = usAllocateNumbers.validateByDefaultNumberCheckboxIsDisplayed(secondNumber);
		boolean expectedResult = true;
		assertEquals(actualResult, expectedResult);
		
		
		
		
	}
	
	
	

}
