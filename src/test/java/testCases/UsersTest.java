package testCases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import pages.AddNumber;
import pages.InviteUser;
import pages.LoginPage;
import pages.NumberSettingPage;
import pages.RegistrationPage;
import pages.UsersPage;
import utilsFile.PropertiesFile;
import utilsFile.Retry;
import utilsFile.Utilitylib;

public class UsersTest {

	UsersPage userPage;
	InviteUser inviteUser;
	NumberSettingPage numberSettingPage;
	AddNumber addNumber;
	LoginPage loginPage;
	WebDriver driver;
	static Utilitylib excel;
	RegistrationPage registrationPage;
	PropertiesFile url;

	public UsersTest() throws Exception {
		url = new PropertiesFile();
		excel = new Utilitylib("..\\Web\\src\\main\\java\\config\\Signup.xlsx");
	}

	@BeforeMethod
	public void initialization() throws IOException {

		driver = TestBase.init();

		userPage = PageFactory.initElements(driver, UsersPage.class);
		inviteUser = PageFactory.initElements(driver, InviteUser.class);
		numberSettingPage = PageFactory.initElements(driver, NumberSettingPage.class);
		addNumber = PageFactory.initElements(driver, AddNumber.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = result.getTestContext().getName()+" - Fail";
			Utilitylib.Screenshot(driver, testname,"Fail "+result.getMethod().getMethodName());
		} else {
			String testname = result.getTestContext().getName()+" - Pass";
			Utilitylib.Screenshot(driver, testname,"Pass "+result.getMethod().getMethodName());
		}
		driver.quit();
	}

	public String signupAndSignin() throws Exception {
		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		driver.get(url.signUp());
		registrationPage.enterFullname("tstAutomation");
		registrationPage.enterCompanyName("appitSimple");
		registrationPage.enterMobile("918511695975");
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("12345678");
		registrationPage.clickOnSignupButton();
		boolean actualResultr = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResultr = true;
		assertEquals(actualResultr, expectedResultr);

		registrationPage.ValidateVerifyButtonOnGmail();
//		boolean actualVerifyMsg = registrationPage.accountVerifiedMsg();
//		boolean expectedVerifyMsg = true;
//		assertEquals(actualVerifyMsg, expectedVerifyMsg);
//
//		driver.get(url.signIn());
//		loginPage.enterEmail(email);
//		loginPage.enterPassword("12345678");
//		loginPage.clickOnLogin();

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.closePopup();
		addNumber.closePopup();

		return email;
	}

	public void addNumberAndOpenTheirNumberSettingPage() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();

		String numberBeforePurchased = addNumber.getSeletedNumber();
		addNumber.selectNumber();
		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();
		addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$8.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		String NumberAfterPurchased = addNumber.getNumeberAfterPurchased();
		assertEquals(numberBeforePurchased, NumberAfterPurchased);

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);
	}

	public String inviteUser() throws Exception {
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		return email;
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_Users_page_is_open_successfully() throws Exception {
		signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();

		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_headers_main_user_on_userPage() throws Exception {
		String expectedMainUser = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();

		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String actualMainUser = userPage.validateMainUserOnUserPage();
		assertEquals(actualMainUser, expectedMainUser);

		assertEquals(userPage.validateHeaderText("1"), "Email");
		assertEquals(userPage.validateHeaderText("2"), "Forward to device");
		assertEquals(userPage.validateHeaderText("3"), "Availability");
		assertEquals(userPage.validateHeaderText("4"), "Numbers");
		assertEquals(userPage.validateHeaderText("5"), "Status");

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_multiUsers_on_users_page() throws Exception {
		String expectedMainUser = signupAndSignin();
		addNumberAndOpenTheirNumberSettingPage();
		String expectedSubUser = inviteUser();
		driver.get(url.signIn());
		Thread.sleep(2000);
		userPage.clickOnUsersLink();

		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String actualMainUser = userPage.validateMainUserOnUserPage();
		assertEquals(actualMainUser, expectedMainUser);

		String actualSubUser = userPage.validateSubUserOnUserPage();
		assertEquals(actualSubUser, expectedSubUser);

		assertEquals(userPage.validateDeleteButtonSize(), 1);
		assertEquals(userPage.validateSettingButtonSize(), 2);

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_user_setting_page_open_successfully() throws Exception {
		signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();

		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		userPage.clickOnSettingIcon();
		String actualTitle1 = userPage.ValidateUserSettingPageTitle();
		String expectedTitle1 = "User Settings | Callhippo.com";
		assertEquals(actualTitle1, expectedTitle1);

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_delete_subUser_and_login() throws Exception {
		String expectedMainUser = signupAndSignin();
		addNumberAndOpenTheirNumberSettingPage();
		String expectedSubUser = inviteUser();
		driver.get(url.signIn());
		Thread.sleep(2000);
		userPage.clickOnUsersLink();

		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String actualMainUser = userPage.validateMainUserOnUserPage();
		assertEquals(actualMainUser, expectedMainUser);

		String actualSubUser = userPage.validateSubUserOnUserPage();
		assertEquals(actualSubUser, expectedSubUser);

		assertEquals(userPage.validateDeleteButtonSize(), 1);
		assertEquals(userPage.validateSettingButtonSize(), 2);

		userPage.clickOnDeleteIcon();
		userPage.clickOnYesButtonOfDeletePopup();

		String actualValidationMsg = userPage.validateDeleteUserValidationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);

		userPage.clickOnLogoutLink();

		String actualResult = loginPage.getTitle();
		String expectedResult = "Login | Callhippo.com";
		assertEquals(actualResult, expectedResult);

		loginPage.enterEmail(expectedSubUser);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();

		String actualEmailValidationMsg = userPage.validateloginErrorMessage();
		String expectedEmailValidationMsg = "Your account has been inactive. Please contact us at support@callhippo.com";
		assertEquals(actualEmailValidationMsg, expectedEmailValidationMsg);

	}

}
