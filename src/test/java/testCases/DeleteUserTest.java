package testCases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import pages.AddNumber;
import pages.CreateTeam;
import pages.DeleteUser;
import pages.InviteUser;
import pages.LoginPage;
import pages.NSAllocation;
import pages.NSivr;
import pages.NumberSettingPage;
import pages.RegistrationPage;
import pages.USAllocateNumbers;
import pages.USUser;
import pages.UsersPage;
import utilsFile.PropertiesFile;
import utilsFile.Retry;
import utilsFile.Utilitylib;

public class DeleteUserTest {

	DeleteUser deleteUser;
	NSivr nsIVR;
	NSAllocation nsAllocation;
	CreateTeam createTeam;
	InviteUser inviteUser;
	NumberSettingPage numberSettingPage;
	USAllocateNumbers usAllocateNumbers;
	USUser usUser;
	UsersPage userPage;
	AddNumber addNumber;
	LoginPage loginPage;
	WebDriver driver;
	static Utilitylib excel;
	RegistrationPage registrationPage;
	PropertiesFile url;

	public DeleteUserTest() throws Exception {
		url = new PropertiesFile();
		excel = new Utilitylib("..\\Web\\src\\main\\java\\config\\Signup.xlsx");
	}

	@BeforeMethod
	public void initialization() throws IOException {

		driver = TestBase.init();

		deleteUser = PageFactory.initElements(driver, DeleteUser.class);
		nsAllocation = PageFactory.initElements(driver, NSAllocation.class);
		createTeam = PageFactory.initElements(driver, CreateTeam.class);
		nsIVR = PageFactory.initElements(driver, NSivr.class);
		inviteUser = PageFactory.initElements(driver, InviteUser.class);
		numberSettingPage = PageFactory.initElements(driver, NumberSettingPage.class);
		usAllocateNumbers = PageFactory.initElements(driver, USAllocateNumbers.class);
		usUser = PageFactory.initElements(driver, USUser.class);
		userPage = PageFactory.initElements(driver, UsersPage.class);
		addNumber = PageFactory.initElements(driver, AddNumber.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = result.getTestContext().getName()+" - Fail";
			Utilitylib.Screenshot(driver, testname,"Fail "+result.getMethod().getMethodName());
		} else {
			String testname = result.getTestContext().getName()+" - Pass";
			Utilitylib.Screenshot(driver, testname,"Pass "+result.getMethod().getMethodName());
		}
		//driver.quit();
	}

	public String signupAndSignin() throws Exception {
		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		driver.get(url.signUp());
		registrationPage.enterFullname("tstAutomation");
		registrationPage.enterCompanyName("appitSimple");
		registrationPage.enterMobile("918511695975");
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("12345678");
		registrationPage.clickOnSignupButton();
		boolean actualResultr = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResultr = true;
		assertEquals(actualResultr, expectedResultr);

		registrationPage.ValidateVerifyButtonOnGmail();
//		boolean actualVerifyMsg = registrationPage.accountVerifiedMsg();
//		boolean expectedVerifyMsg = true;
//		assertEquals(actualVerifyMsg, expectedVerifyMsg);
//
//		driver.get(url.signIn());
//		loginPage.enterEmail(email);
//		loginPage.enterPassword("12345678");
//		loginPage.clickOnLogin();

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.closePopup();
		addNumber.closePopup();

		return email;
	}
	
	public String addNumberAndOpenTheirNumberSettingPage() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();

//		addNumber.selectSearchByTollfree();
//		addNumber.clickOnSearchButton();

		String numberBeforePurchased = addNumber.getSeletedNumber();
		addNumber.selectNumber();
		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();
		addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$8.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		String NumberAfterPurchased = addNumber.getNumeberAfterPurchased();
		assertEquals(numberBeforePurchased, NumberAfterPurchased);

		return NumberAfterPurchased;
		// numberSettingPage.invisiblityOfValidationMessage();

	}
	
	public void addNumberWithBronzePlan() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber();

		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();

		addNumber.selectMontly();
		addNumber.selectBronze();
		addNumber.clickOnCheckoutButton();

		// addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		// numberSettingPage.invisiblityOfValidationMessage();

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);

	}
	
	public String verify_invite_User_successfully() throws Exception {

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);
		
		return email;

	}
	
	public void add_invited_user_in_IVR() throws Exception {
		//Thread.sleep(2000);
		addNumber.clickOnNumberLinkAfterPurchasedNumber();
		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);
		
		nsIVR.clickOnIVRSection();
		nsIVR.clickonIVRToggle();

		nsIVR.enterPressDigits("1");
		nsIVR.selectValueFromActionDropdown("Invited User1");
		nsIVR.clickOnSaveIcon();

		nsIVR.waitForDigitAddedSuccessfullyMessage("1");
		String acutualErrorValidationMsg = numberSettingPage.validationMessage();
		String expectedErrorValidationMsg = "IVR for digit 1 is added successfully";
		assertEquals(acutualErrorValidationMsg, expectedErrorValidationMsg);
	}
	
	public void createTeam() throws IOException {
		driver.get(url.signIn());
		createTeam.clickOnTeamLink();
		createTeam.clickOnCreateTeamLink();
		createTeam.enterTeamName("Team 1");
		createTeam.clickOnSimultaneouslyRedioButton();
		createTeam.selectUser1();
		createTeam.clickOnYesButton();
		createTeam.selectUser2();
		createTeam.clickOnYesButton();
		createTeam.closeCreditPopup();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {

		}
		createTeam.clickOnCreateButton();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {

		}
		createTeam.clickOnYesButton();

		createTeam.waitForvalidationMessage();
		String acutualSuccessfulValidationMsg = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg = "Team created successfully";
		assertEquals(acutualSuccessfulValidationMsg, expectedSuccessfulValidationMsg);

	}
	
	public void deallocateUser() throws Exception {

		driver.get(url.signIn());
		addNumber.clickOnNumberLinkAfterPurchasedNumber();
		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);

		nsAllocation.clickOnUser2();
		String actualPopupMessage = nsAllocation.ValidatPopupText();
		String expectedPopupMessage = "Are you sure you want to deallocate this user ?";
		assertEquals(actualPopupMessage, expectedPopupMessage);

		nsAllocation.clickOnPopupYesButton();

		nsAllocation.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "User deallocated successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);
	}
	
	
	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_Delete_User_while_user_allocated_number() throws Exception {
	
		signupAndSignin();
		addNumberWithBronzePlan();
		String email = verify_invite_User_successfully();
		driver.get(url.signIn());
		
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult = deleteUser.Validate_popup_texts();
		String expectedResult = "Number 1 will be deallocated from this user. Are you sure you want to delete this user ?";
		assertEquals(actualResult, expectedResult);
		
		deleteUser.clickOnNoButton();
		Thread.sleep(1000);
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult1 = deleteUser.Validate_popup_texts();
		String expectedResult1 = "Number 1 will be deallocated from this user. Are you sure you want to delete this user ?";
		assertEquals(actualResult1, expectedResult1);
		
		deleteUser.clickOnYesButton();
		String actualValidationMsg = deleteUser.validationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
		
	
	}
	
	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_Delete_User_while_user_allocated_Number_and_IVR() throws Exception {
	
		signupAndSignin();
		addNumberWithBronzePlan();
		String email = verify_invite_User_successfully();
		driver.get(url.signIn());
		add_invited_user_in_IVR();
		
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult = deleteUser.Validate_popup_texts();
		String expectedResult = "Number 1 will be deallocated from this user. Are you sure you want to delete this user ?";
		assertEquals(actualResult, expectedResult);
		
		deleteUser.clickOnNoButton();
		Thread.sleep(1000);
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult1 = deleteUser.Validate_popup_texts();
		String expectedResult1 = "Number 1 will be deallocated from this user. Are you sure you want to delete this user ?";
		assertEquals(actualResult1, expectedResult1);
		
		deleteUser.clickOnYesButton();
		String actualValidationMsg = deleteUser.validationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
		
	
	}
	
	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_Delete_User_while_user_allocated_Number_and_Team() throws Exception {
	
		signupAndSignin();
		addNumberWithBronzePlan();
		String email = verify_invite_User_successfully();
		createTeam();
		driver.get(url.signIn());
		
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult = deleteUser.Validate_popup_texts();
		String expectedResult = "Number 1 will be deallocated from this user. Are you sure you want to delete this user ?";
		assertEquals(actualResult, expectedResult);
		
		deleteUser.clickOnNoButton();
		Thread.sleep(1000);
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult1 = deleteUser.Validate_popup_texts();
		String expectedResult1 = "Number 1 will be deallocated from this user. Are you sure you want to delete this user ?";
		assertEquals(actualResult1, expectedResult1);
		
		deleteUser.clickOnYesButton();
		String actualValidationMsg = deleteUser.validationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
		
	
	}
	
	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_Delete_User_while_user_allocated_number_Team_and_IVR () throws Exception {
	
		signupAndSignin();
		addNumberWithBronzePlan();
		String email = verify_invite_User_successfully();
		createTeam();
		driver.get(url.signIn());
		add_invited_user_in_IVR();
		
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult = deleteUser.Validate_popup_texts();
		String expectedResult = "Number 1 will be deallocated from this user. Are you sure you want to delete this user ?";
		assertEquals(actualResult, expectedResult);
		
		deleteUser.clickOnNoButton();
		Thread.sleep(1000);
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult1 = deleteUser.Validate_popup_texts();
		String expectedResult1 = "Number 1 will be deallocated from this user. Are you sure you want to delete this user ?";
		assertEquals(actualResult1, expectedResult1);
		
		deleteUser.clickOnYesButton();
		String actualValidationMsg = deleteUser.validationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
	
	}
	
	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_Delete_User_while_user_not_allocated_in_Number_Team_IVR() throws Exception {
	
		signupAndSignin();
		addNumberWithBronzePlan();
		String email = verify_invite_User_successfully();
		deallocateUser();
		driver.get(url.signIn());
		
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult = deleteUser.Validate_popup_texts();
		String expectedResult = "Are you sure you want to delete this user ?";
		assertEquals(actualResult, expectedResult);
		
		deleteUser.clickOnNoButton();
		Thread.sleep(1000);
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult1 = deleteUser.Validate_popup_texts();
		String expectedResult1 = "Are you sure you want to delete this user ?";
		assertEquals(actualResult1, expectedResult1);
		
		deleteUser.clickOnYesButton();
		String actualValidationMsg = deleteUser.validationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
		
	
	}
	
	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_Delete_User_while_user_allocated_IVR() throws Exception {
	
		signupAndSignin();
		addNumberWithBronzePlan();
		String email = verify_invite_User_successfully();
		driver.get(url.signIn());
		add_invited_user_in_IVR();
		deallocateUser();
		
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult = deleteUser.Validate_popup_texts();
		String expectedResult = "Are you sure you want to delete this user ?";
		assertEquals(actualResult, expectedResult);
		
		deleteUser.clickOnNoButton();
		Thread.sleep(1000);
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult1 = deleteUser.Validate_popup_texts();
		String expectedResult1 = "Are you sure you want to delete this user ?";
		assertEquals(actualResult1, expectedResult1);
		
		deleteUser.clickOnYesButton();
		String actualValidationMsg = deleteUser.validationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
		
	
	}
	
	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_Delete_User_while_user_allocated_in_Team() throws Exception {
	
		signupAndSignin();
		addNumberWithBronzePlan();
		String email = verify_invite_User_successfully();
		createTeam();
		deallocateUser();
		driver.get(url.signIn());
		
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult = deleteUser.Validate_popup_texts();
		String expectedResult = "Are you sure you want to delete this user ?";
		assertEquals(actualResult, expectedResult);
		
		deleteUser.clickOnNoButton();
		Thread.sleep(1000);
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult1 = deleteUser.Validate_popup_texts();
		String expectedResult1 = "Are you sure you want to delete this user ?";
		assertEquals(actualResult1, expectedResult1);
		
		deleteUser.clickOnYesButton();
		String actualValidationMsg = deleteUser.validationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
		
	
	}
	
	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_Delete_User_while_user_allocated_Team_and_IVR () throws Exception {
	
		signupAndSignin();
		addNumberWithBronzePlan();
		String email = verify_invite_User_successfully();
		createTeam();
		driver.get(url.signIn());
		add_invited_user_in_IVR();
		deallocateUser();
		
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult = deleteUser.Validate_popup_texts();
		String expectedResult = "Are you sure you want to delete this user ?";
		assertEquals(actualResult, expectedResult);
		
		deleteUser.clickOnNoButton();
		Thread.sleep(1000);
		deleteUser.clickOnDeleteUserOf(email);
		String actualResult1 = deleteUser.Validate_popup_texts();
		String expectedResult1 = "Are you sure you want to delete this user ?";
		assertEquals(actualResult1, expectedResult1);
		
		deleteUser.clickOnYesButton();
		String actualValidationMsg = deleteUser.validationMessage();
		String expectedValidationMsg = "User deleted successfully";
		assertEquals(actualValidationMsg, expectedValidationMsg);
	
	}
	
	
}
