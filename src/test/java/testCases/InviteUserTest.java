package testCases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import pages.AddNumber;
import pages.InviteUser;
import pages.LoginPage;
import pages.NSAllocation;
import pages.NumberSettingPage;
import pages.RegistrationPage;
import utilsFile.PropertiesFile;
import utilsFile.Retry;
import utilsFile.Utilitylib;

public class InviteUserTest {

	InviteUser inviteUser;
	NumberSettingPage numberSettingPage;
	AddNumber addNumber;
	LoginPage loginPage;
	WebDriver driver;
	static Utilitylib excel;
	RegistrationPage registrationPage;
	PropertiesFile url;

	public InviteUserTest() throws Exception {
		url = new PropertiesFile();
		excel = new Utilitylib("..\\Web\\src\\main\\java\\config\\Signup.xlsx");
	}

	@BeforeMethod
	public void initialization() throws IOException {

		driver = TestBase.init();
		// driver.get(url.signIn());

		inviteUser = PageFactory.initElements(driver, InviteUser.class);
		numberSettingPage = PageFactory.initElements(driver, NumberSettingPage.class);
		addNumber = PageFactory.initElements(driver, AddNumber.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = result.getTestContext().getName() + " - Fail";
			Utilitylib.Screenshot(driver, testname, "Fail " + result.getMethod().getMethodName());
		} else {
			String testname = result.getTestContext().getName() + " - Pass";
			Utilitylib.Screenshot(driver, testname, "Pass " + result.getMethod().getMethodName());
		}
		 driver.quit();
	}

	public String signupAndSignin() throws Exception {
		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		driver.get(url.signUp());
		registrationPage.enterFullname("tstAutomation");
		registrationPage.enterCompanyName("appitSimple");
		registrationPage.enterMobile("918511695975");
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("12345678");
		registrationPage.clickOnSignupButton();
		boolean actualResultr = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResultr = true;
		// SoftAssert sa = new SoftAssert();
		assertEquals(actualResultr, expectedResultr);

		registrationPage.ValidateVerifyButtonOnGmail();
//		boolean actualVerifyMsg = registrationPage.accountVerifiedMsg();
//		boolean expectedVerifyMsg = true;
//		assertEquals(actualVerifyMsg, expectedVerifyMsg);

//		driver.get(url.signIn());
//		loginPage.enterEmail(email);
//		loginPage.enterPassword("12345678");
//		loginPage.clickOnLogin();

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.closePopup();
		addNumber.closePopup();

		return email;
	}

	public void addMonthlyNumberPlanAlreadySelected() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber2();

		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();

		addNumber.waitForAddNumberPage();

		addNumber.clickOnSaveButton();

		String actualMessage1 = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage1 = "Number added successfully";
		assertEquals(actualMessage1, expectedMessage1);
	}

	public void addNumberAlreadySilverPlanSelected() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber();

		addNumber.clickOnAddButton();

		addNumber.waitForAddNumberPage();

		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);
	}

	public void addAnnuallyNumberPlanAlreadySelected() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber2();

		addNumber.selectNumberAnnualyPlan();
		addNumber.clickOnPayButton();

		addNumber.waitForAddNumberPage();

		addNumber.clickOnSaveButton();

		String actualMessage1 = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage1 = "Number added successfully";
		assertEquals(actualMessage1, expectedMessage1);
	}

	public void addNumberAndOpenTheirNumberSettingPage() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();

//		addNumber.selectSearchByTollfree();
//		addNumber.clickOnSearchButton();

		String numberBeforePurchased = addNumber.getSeletedNumber();
		addNumber.selectNumber();
		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();
		addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$8.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		String NumberAfterPurchased = addNumber.getNumeberAfterPurchased();
		assertEquals(numberBeforePurchased, NumberAfterPurchased);

		// numberSettingPage.invisiblityOfValidationMessage();

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);
	}

	public void addNumberWithBronzePlan() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber();

		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();

		addNumber.selectMontly();
		addNumber.selectBronze();
		addNumber.clickOnCheckoutButton();

		// addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		// numberSettingPage.invisiblityOfValidationMessage();

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);

	}

	public void inviteUser() throws Exception {
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
//		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
//		String expectedActivatedMessage = "Your account has been activated.";
//		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_invite_User_without_adding_Number() throws Exception {

		signupAndSignin();
		// addNumberAndOpenTheirNumberSettingPage();
		Thread.sleep(2000);

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		inviteUser.waitForAtleastOneNumberValidation();
		String actualValidationMessage = numberSettingPage.validationMessage();
		String expectedValidationMessage = "Before inviting user you must have atleast one number.";
		assertEquals(actualValidationMessage, expectedValidationMessage);
	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_invite_User_page_Open_successfully() throws Exception {

		signupAndSignin();
		addNumberAndOpenTheirNumberSettingPage();

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);
	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_emailAddressValidation() throws Exception {

		signupAndSignin();
		addNumberAndOpenTheirNumberSettingPage();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		inviteUser.clickOnSaveButton();
		inviteUser.waitForRequiredFieldValidationMsg();
		String actualRequireFieldValidationMessage = numberSettingPage.validationMessage();
		String expectedRequireFieldValidationMessage = "Please enter the Email Id.";
		assertEquals(actualRequireFieldValidationMessage, expectedRequireFieldValidationMessage);
		inviteUser.closeValidationPopup();

		inviteUser.clearEmail();
		inviteUser.enterEmail("test");
		inviteUser.clickOnSaveButton();
		inviteUser.waitForinvalidEmailValidationMsg();
		String actualInvalidEmailValidationMessage = numberSettingPage.validationMessage();
		String expectedInvalidEmailValidationMessage = "Please enter the valid Email Id.";
		assertEquals(actualInvalidEmailValidationMessage, expectedInvalidEmailValidationMessage);
		inviteUser.closeValidationPopup();

		inviteUser.clearEmail();
		inviteUser.enterEmail("test@");
		inviteUser.clickOnSaveButton();
		inviteUser.waitForinvalidEmailValidationMsg();
		String actualInvalidEmailValidationMessage1 = numberSettingPage.validationMessage();
		String expectedInvalidEmailValidationMessage1 = "Please enter the valid Email Id.";
		assertEquals(actualInvalidEmailValidationMessage1, expectedInvalidEmailValidationMessage1);
		inviteUser.closeValidationPopup();

		inviteUser.clearEmail();
		inviteUser.enterEmail("test@gmail");
		inviteUser.clickOnSaveButton();
		inviteUser.waitForinvalidEmailValidationMsg();
		String actualInvalidEmailValidationMessage2 = numberSettingPage.validationMessage();
		String expectedInvalidEmailValidationMessage2 = "Please enter the valid Email Id.";
		assertEquals(actualInvalidEmailValidationMessage2, expectedInvalidEmailValidationMessage2);
		inviteUser.closeValidationPopup();

		inviteUser.clearEmail();
		inviteUser.enterEmail("test@gmail.");
		inviteUser.clickOnSaveButton();
		inviteUser.waitForinvalidEmailValidationMsg();
		String actualInvalidEmailValidationMessage3 = numberSettingPage.validationMessage();
		String expectedInvalidEmailValidationMessage3 = "Please enter the valid Email Id.";
		assertEquals(actualInvalidEmailValidationMessage3, expectedInvalidEmailValidationMessage3);
		inviteUser.closeValidationPopup();

		inviteUser.clearEmail();
		inviteUser.enterEmail("test@gmail.com");
		inviteUser.clickOnSaveButton();

		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2("test@gmail.com");
		inviteUser.clickOnSaveButton();
		inviteUser.waitForEmailAlreadyExistValidationMessage();
		String actualEmailAlreadyExistValidationMessage = numberSettingPage.validationMessage();
		String expectedEmailAlreadyExistValidationMessage = "Email already exists";
		assertEquals(actualEmailAlreadyExistValidationMessage, expectedEmailAlreadyExistValidationMessage);
		inviteUser.closeValidationPopup();

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_invite_User_pending_Status_and_SMS_Button() throws Exception {

		signupAndSignin();
		addNumberAndOpenTheirNumberSettingPage();

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_invited_User_Register_page_fields_validation() throws Exception {

		signupAndSignin();
		addNumberAndOpenTheirNumberSettingPage();

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		inviteUser.ValidateInvitatonLink();
		// fulName field validation
		inviteUser.enterFullName("Invited User1");
		inviteUser.clearFullName();
		assertEquals(inviteUser.validateNameRequireFieldValidation(), "name is required.");

		inviteUser.enterFullName("1234");
		assertEquals(inviteUser.validateNameIsTooShortValidation(), "name is too short.");

		inviteUser.clearFullName();
		inviteUser.enterFullName("!@#D");
		assertEquals(inviteUser.validateNameIsTooShortValidation(), "name is too short.");

		// Password field Validation
		inviteUser.enterPassword("12345678");
		inviteUser.clearPassword();
		assertEquals(inviteUser.validatePasswordRequiredFieldValidation(), "Password is required.");

		inviteUser.enterPassword("1234567");
		assertEquals(inviteUser.validatePasswordIsTooShortValidation(), "Password is too short.");

		inviteUser.clearPassword();
		inviteUser.enterPassword("12We#");
		assertEquals(inviteUser.validatePasswordIsTooShortValidation(), "Password is too short.");

		// Confirm Password field validation

		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clearConfirmPassword();
		assertEquals(inviteUser.validateConfirmPasswordRequiredFieldValidation(), "Confirm password is required.");

		inviteUser.enterconfirmPassword("1234567");
		assertEquals(inviteUser.validateConfirmPasswordIsTooShortValidation(), "Confirm password is too short.");

		inviteUser.clearConfirmPassword();
		inviteUser.enterconfirmPassword("12We#");
		assertEquals(inviteUser.validateConfirmPasswordIsTooShortValidation(), "Confirm password is too short.");

		inviteUser.clearFullName();
		inviteUser.clearPassword();
		inviteUser.clearConfirmPassword();

		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678455");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateMismatchPasswordValidation();
		String expectedActivatedMessage = "Password and confirm password does not matched";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_Expired_invite_user_Link() throws Exception {

		signupAndSignin();
		addNumberAndOpenTheirNumberSettingPage();

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		inviteUser.ValidateInvitatonLink();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		inviteUser.ValidateInvitatonLink();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualExpiredValidationMsg = inviteUser.validateExpiredInvitationLinkValidation();
		String expectedExpiredValidationMsg = "Sorry this invitation request has expired. Please contact your admin user.";
		assertEquals(actualExpiredValidationMsg, expectedExpiredValidationMsg);

	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_invite_more_than_one_users_in_basic_plan_After_Invited_User_moved_to_Bronze() throws Exception {

		signupAndSignin();
		String date = excel.date();
		addNumberAndOpenTheirNumberSettingPage();

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date1 = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";
		String email2 = "jayadip+" + date1 + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2(email2);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		String actualTextResult = inviteUser.validateExededUserLimitPopupText();
		String expectedTextResult = "You are crossing the limit for maximum users in the basic plan (2 users).By adding the next user your account will be subscribed to the Bronze plan. Are you sure you want to opt for the Bronze Plan.";
		assertEquals(actualTextResult, expectedTextResult);

		inviteUser.clickOnNoButtonOfExededPopup();

		inviteUser.clickOnInviteButton();
		inviteUser.clickOnYesButtonOfExededPopup();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		String actualStatus1 = inviteUser.ValidatePendingStatusOf(email2);
		String expectedStatus1 = "Pending";
		assertEquals(actualStatus1, expectedStatus1);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.clickOnMessageButton(email2);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg2 = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg2, expectedSentInvitationValidationMsg2);

	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_invite_two_Users_and_accept_ivitation() throws Exception {

		String mainEmail = signupAndSignin();
		String date = excel.date();
		addNumberAndOpenTheirNumberSettingPage();

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date1 = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";
		String email2 = "jayadip+" + date1 + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2(email2);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		String actualTextResult = inviteUser.validateExededUserLimitPopupText();
		String expectedTextResult = "You are crossing the limit for maximum users in the basic plan (2 users).By adding the next user your account will be subscribed to the Bronze plan. Are you sure you want to opt for the Bronze Plan.";
		assertEquals(actualTextResult, expectedTextResult);

		inviteUser.clickOnNoButtonOfExededPopup();

		inviteUser.clickOnInviteButton();
		inviteUser.clickOnYesButtonOfExededPopup();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		String actualStatus1 = inviteUser.ValidatePendingStatusOf(email2);
		String expectedStatus1 = "Pending";
		assertEquals(actualStatus1, expectedStatus1);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.clickOnMessageButton(email2);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg2 = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg2, expectedSentInvitationValidationMsg2);

		// Accept invitation from gmail account

		inviteUser.ValidateSecondLastInvitationButton();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		inviteUser.ValidateInvitatonLink();
		inviteUser.enterFullName("Invited User2");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage2 = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage2 = "Your account has been activated.";
		assertEquals(actualActivatedMessage2, expectedActivatedMessage2);

		addNumber.clickOnMyAccountMenu();
		inviteUser.clickOnLogout();

		loginPage.enterEmail(mainEmail);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();

		// validate Active status of invited Users

		// driver.get(url.signIn());
		Thread.sleep(2000);
		inviteUser.clickOnUsersLink();
		String actualStatus2 = inviteUser.ValidateActiveStatusOf(email);
		String expectedStatus2 = "Active";
		assertEquals(actualStatus2, expectedStatus2);

		String actualStatus3 = inviteUser.ValidateActiveStatusOf(email2);
		String expectedStatus3 = "Active";
		assertEquals(actualStatus3, expectedStatus3);

		// verify invited User name in setting page

		inviteUser.clickOnSettingIconOf(email);
		String actualSubUserName = inviteUser.validateInvitedUserName();
		String expectedSubUserName = "Invited User1";
		assertEquals(actualSubUserName, expectedSubUserName);

		driver.navigate().back();
		inviteUser.clickOnSettingIconOf(email2);
		String actualSubUserName1 = inviteUser.validateInvitedUserName();
		String expectedSubUserName1 = "Invited User2";
		assertEquals(actualSubUserName1, expectedSubUserName1);

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void verify_invite_User_successfully() throws Exception {

		signupAndSignin();
		addNumberAndOpenTheirNumberSettingPage();

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void verify_invite_two_users_free_in_basicPlan() throws Exception {

		String mainEmail = signupAndSignin();
		String date = excel.date();
		addNumberAndOpenTheirNumberSettingPage();

		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.clickOnMyAccountMenu();
		inviteUser.clickOnLogout();

		loginPage.enterEmail(mainEmail);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();

		// driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$8.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void Verify_invite_Users_with_Bronze_monthly_plan() throws Exception {
		String mainEmail = signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$10.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addMonthlyNumberPlanAlreadySelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$8.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.clickOnMyAccountMenu();
		inviteUser.clickOnLogout();

		loginPage.enterEmail(mainEmail);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();

		// driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$10.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void Verify_invite_Users_with_Bronze_Annually_plan() throws Exception {
		String mainEmail = signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$96.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$96.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addAnnuallyNumberPlanAlreadySelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$72.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.clickOnMyAccountMenu();
		inviteUser.clickOnLogout();

		loginPage.enterEmail(mainEmail);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();

		// driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$96.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void Verify_invite_Users_with_Silver_monthly_plan() throws Exception {
		String mainEmail = signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserSilverPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$18.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addNumberAlreadySilverPlanSelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$18.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.clickOnMyAccountMenu();
		inviteUser.clickOnLogout();

		loginPage.enterEmail(mainEmail);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();
		// driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$18.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void Verify_invite_Users_with_Silver_Annually_plan() throws Exception {
		String mainEmail = signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserSilverPlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$180.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$180.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addNumberAlreadySilverPlanSelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$180.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.clickOnMyAccountMenu();
		inviteUser.clickOnLogout();

		loginPage.enterEmail(mainEmail);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();
		// driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$180.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void Verify_invite_Users_with_Platinum_monthly_plan() throws Exception {
		String mainEmail = signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$40.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$40.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addNumberAlreadySilverPlanSelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$40.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.clickOnMyAccountMenu();
		inviteUser.clickOnLogout();

		loginPage.enterEmail(mainEmail);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();
		// driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$40.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void Verify_invite_Users_with_Platinum_Annually_plan() throws Exception {
		String mainEmail = signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$420.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$420.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addNumberAlreadySilverPlanSelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$420.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";

		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.ValidateVerifyButtonOnGmail();
		inviteUser.enterFullName("Invited User1");
		inviteUser.enterPassword("12345678");
		inviteUser.enterconfirmPassword("12345678");
		inviteUser.clickOnSubmitButton();
		String actualActivatedMessage = inviteUser.validateAccountActivatedMessage();
		String expectedActivatedMessage = "Your account has been activated.";
		assertEquals(actualActivatedMessage, expectedActivatedMessage);

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.clickOnMyAccountMenu();
		inviteUser.clickOnLogout();

		loginPage.enterEmail(mainEmail);
		loginPage.enterPassword("12345678");
		loginPage.clickOnLogin();
		// driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$420.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void Verify_invite_multiple_Users_with_Silver_monthly_plan() throws Exception {
		signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserSilverPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$18.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addNumberAlreadySilverPlanSelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$18.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		String date1 = excel.date();

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";
		String email2 = "jayadip+" + date1 + "@callhippo.com";
		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2(email2);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		// inviteUser.closeAddCreditPopup();
		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		String actualStatus1 = inviteUser.ValidatePendingStatusOf(email2);
		String expectedStatus1 = "Pending";
		assertEquals(actualStatus1, expectedStatus1);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.clickOnMessageButton(email2);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg2 = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg2, expectedSentInvitationValidationMsg2);

		// driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$36.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void Verify_invite_multiple_Users_with_Silver_Annually_plan() throws Exception {
		signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserSilverPlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$180.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$180.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addNumberAlreadySilverPlanSelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$180.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		String date1 = excel.date();

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";
		String email2 = "jayadip+" + date1 + "@callhippo.com";
		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2(email2);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		// inviteUser.closeAddCreditPopup();
		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		String actualStatus1 = inviteUser.ValidatePendingStatusOf(email2);
		String expectedStatus1 = "Pending";
		assertEquals(actualStatus1, expectedStatus1);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.clickOnMessageButton(email2);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg2 = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg2, expectedSentInvitationValidationMsg2);

		driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$360.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void Verify_invite_multiple_Users_with_Platinum_monthly_plan() throws Exception {
		signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$40.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$40.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addNumberAlreadySilverPlanSelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$40.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		String date1 = excel.date();

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";
		String email2 = "jayadip+" + date1 + "@callhippo.com";
		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2(email2);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		// inviteUser.closeAddCreditPopup();
		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		String actualStatus1 = inviteUser.ValidatePendingStatusOf(email2);
		String expectedStatus1 = "Pending";
		assertEquals(actualStatus1, expectedStatus1);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.clickOnMessageButton(email2);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg2 = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg2, expectedSentInvitationValidationMsg2);

		driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$80.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void Verify_invite_multiple_Users_with_Platinum_Annually_plan() throws Exception {
		signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$420.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$420.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addNumberAlreadySilverPlanSelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		assertEquals(inviteUser.customerPortalpriceEntrySize(), 1);
		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$420.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		String date1 = excel.date();

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";
		String email2 = "jayadip+" + date1 + "@callhippo.com";
		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2(email2);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		// inviteUser.closeAddCreditPopup();
		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		String actualStatus1 = inviteUser.ValidatePendingStatusOf(email2);
		String expectedStatus1 = "Pending";
		assertEquals(actualStatus1, expectedStatus1);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.clickOnMessageButton(email2);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg2 = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg2, expectedSentInvitationValidationMsg2);

		driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$840.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void Verify_invite_multiple_Users_with_Bronze_monthly_plan() throws Exception {
		signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$10.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addMonthlyNumberPlanAlreadySelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$8.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);
		String date = excel.date();
		driver.get(url.signIn());
		Thread.sleep(2000);

		String date1 = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";
		String email2 = "jayadip+" + date1 + "@callhippo.com";
		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2(email2);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		// inviteUser.closeAddCreditPopup();
		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		String actualStatus1 = inviteUser.ValidatePendingStatusOf(email2);
		String expectedStatus1 = "Pending";
		assertEquals(actualStatus1, expectedStatus1);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.clickOnMessageButton(email2);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg2 = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg2, expectedSentInvitationValidationMsg2);

		driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$20.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

	@Test(priority = 22, retryAnalyzer = Retry.class)
	public void Verify_invite_multiple_Users_with_Bronze_Annually_plan() throws Exception {
		signupAndSignin();

		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$96.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice = "$96.00";
		assertEquals(actualLatestPrice, expectedLatestPrice);

		driver.get(url.signIn());
		addAnnuallyNumberPlanAlreadySelected();

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice1 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice1 = "$72.00";
		assertEquals(actualLatestPrice1, expectedLatestPrice1);

		String date1 = excel.date();

		driver.get(url.signIn());
		Thread.sleep(2000);

		String date = excel.date();
		inviteUser.clickOnUsersLink();
		inviteUser.clickOnInviteUserButton();

		String actualTitle = inviteUser.validateTitle();
		String expectedTitle = "Invite User | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		String email = "jayadip+" + date + "@callhippo.com";
		String email2 = "jayadip+" + date1 + "@callhippo.com";
		inviteUser.enterEmail(email);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnAddButton();
		inviteUser.enterEmail2(email2);
		inviteUser.clickOnSaveButton();
		inviteUser.clickOnNumberCheckbox();
		inviteUser.clickOnInviteButton();

		// inviteUser.closeAddCreditPopup();
		inviteUser.waitUntilSuccessfulValidationMsgDisplay();
		String acutualSuccessfulValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSuccessfulValidationMsg2 = "Your invitation sent successfully";
		assertEquals(acutualSuccessfulValidationMsg2, expectedSuccessfulValidationMsg2);

		String actualStatus = inviteUser.ValidatePendingStatusOf(email);
		String expectedStatus = "Pending";
		assertEquals(actualStatus, expectedStatus);

		String actualStatus1 = inviteUser.ValidatePendingStatusOf(email2);
		String expectedStatus1 = "Pending";
		assertEquals(actualStatus1, expectedStatus1);

		inviteUser.clickOnMessageButton(email);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg, expectedSentInvitationValidationMsg);

		inviteUser.clickOnMessageButton(email2);
		inviteUser.waitForSentInvitationValidationMessage();
		String actualSentInvitationValidationMsg2 = numberSettingPage.validationMessage();
		String expectedSentInvitationValidationMsg2 = "An invitation email has been sent to " + email;
		assertEquals(actualSentInvitationValidationMsg2, expectedSentInvitationValidationMsg2);

		driver.get(url.signIn());
		Thread.sleep(2000);

		addNumber.clickOnMyAccountMenu();
		addNumber.clickonAccountDetail();
		addNumber.waitForCustomerPortalPage2();

		String actualLatestPrice2 = addNumber.validateCustomerPortalLatestPrice();
		String expectedLatestPrice2 = "$192.00";
		assertEquals(actualLatestPrice2, expectedLatestPrice2);

	}

}
