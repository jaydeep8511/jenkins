package testCases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import pages.AddNumber;
import pages.LoginPage;
import pages.NumberSettingPage;
import pages.RegistrationPage;
import pages.USAvailability;
import pages.USUser;
import pages.USVoicemail;
import pages.UsersPage;
import utilsFile.PropertiesFile;
import utilsFile.Retry;
import utilsFile.Utilitylib;

public class USAvailabilityTest {

	USAvailability usAvailability;
	USVoicemail usVoicemail;
	USUser usUser;
	UsersPage userPage;
	AddNumber addNumber;
	LoginPage loginPage;
	WebDriver driver;
	static Utilitylib excel;
	RegistrationPage registrationPage;
	PropertiesFile url;

	public USAvailabilityTest() throws Exception {
		url = new PropertiesFile();
		excel = new Utilitylib("..\\Web\\src\\main\\java\\config\\Signup.xlsx");
	}

	@BeforeMethod
	public void initialization() throws IOException {

		driver = TestBase.init();
		
		usAvailability = PageFactory.initElements(driver, USAvailability.class);
		usVoicemail = PageFactory.initElements(driver, USVoicemail.class);
		usUser = PageFactory.initElements(driver, USUser.class);
		userPage = PageFactory.initElements(driver, UsersPage.class);
		addNumber = PageFactory.initElements(driver, AddNumber.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = result.getTestContext().getName()+" - Fail";
			Utilitylib.Screenshot(driver, testname,"Fail "+result.getMethod().getMethodName());
		} else {
			String testname = result.getTestContext().getName()+" - Pass";
			Utilitylib.Screenshot(driver, testname,"Pass "+result.getMethod().getMethodName());
		}
		driver.quit();
	}

	public String signupAndSignin() throws Exception {
		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		driver.get(url.signUp());
		registrationPage.enterFullname("tstAutomation");
		registrationPage.enterCompanyName("appitSimple");
		registrationPage.enterMobile("918511695975");
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("12345678");
		registrationPage.clickOnSignupButton();
		boolean actualResultr = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResultr = true;
		assertEquals(actualResultr, expectedResultr);

		registrationPage.ValidateVerifyButtonOnGmail();
//		boolean actualVerifyMsg = registrationPage.accountVerifiedMsg();
//		boolean expectedVerifyMsg = true;
//		assertEquals(actualVerifyMsg, expectedVerifyMsg);
//
//		driver.get(url.signIn());
//		loginPage.enterEmail(email);
//		loginPage.enterPassword("12345678");
//		loginPage.clickOnLogin();

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.closePopup();
		addNumber.closePopup();

		return email;
	}


	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_with_Update_timeZone() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAvailability.clickOnEditTimeZone();
		String expectedVisibleText = usAvailability.selectTimeZoneOf("(UTC-05:00) Kingston, George Town");
		usAvailability.clickOnSaveButton();
		String actualValidationMessage = usAvailability.validationMessage();
		String expectedValidationMessage = "User updated successfully";
		assertEquals(actualValidationMessage, expectedValidationMessage);
		usAvailability.closeValidationMessage();
		String actualVisibleText = usAvailability.getTimezoneText();
		assertEquals(actualVisibleText, expectedVisibleText);
		
	}
	
	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_Always_opened_button() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAvailability.clickOnAlwaysOpenedButton();
		String actualValidationMessage = usAvailability.validationMessage();
		String expectedValidationMessage = "User updated successfully";
		assertEquals(actualValidationMessage, expectedValidationMessage);
		usAvailability.closeValidationMessage();
		
		
	}
	
	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_Always_Closed_button() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAvailability.clickOnAlwaysClosedButton();
		String actualValidationMessage = usAvailability.validationMessage();
		String expectedValidationMessage = "User updated successfully";
		assertEquals(actualValidationMessage, expectedValidationMessage);
		usAvailability.closeValidationMessage();
	}
	
	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_Custom_button_in_Basic_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAvailability.clickOnCustomButton();
		assertEquals(usAvailability.selectPlanPopupIsDisplay(), true);
	}
	
	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_Custom_button_in_Bronze_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAvailability.clickOnCustomButton();
		assertEquals(usAvailability.selectPlanPopupIsDisplay(), true);
		usAvailability.clickOnSelectPlanButton();
		
		usAvailability.clickOnYourPlanLink();
		
		Thread.sleep(2000);
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		userPage.clickOnUsersLink();
		String actualTitleA = userPage.ValidatePageTitle();
		String expectedTitleA = "Users | Callhippo.com";
		assertEquals(actualTitleA, expectedTitleA);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitleB = usUser.ValidatePageTitle();
		String expectedUserSettingTitleB = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitleB, expectedUserSettingTitleB);
		
		usAvailability.clickOnCustomButton();
		
		String actualValidationMessage = usAvailability.validationMessage();
		String expectedValidationMessage = "User updated successfully";
		assertEquals(actualValidationMessage, expectedValidationMessage);
		usAvailability.closeValidationMessage();
		usAvailability.clickOnCustomTimeLink();
		String actualCustomAvailabilityTitle =usAvailability.validateCustomAvailabilityPage();
		String expectedCustomAvailabilityTitle = "Users - Custom Availability | Callhippo.com";
		assertEquals(actualCustomAvailabilityTitle, expectedCustomAvailabilityTitle);
		
		}
	
	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_Custom_button_in_Silver_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAvailability.clickOnCustomButton();
		assertEquals(usAvailability.selectPlanPopupIsDisplay(), true);
		usAvailability.clickOnSelectPlanButton();
		
		usAvailability.clickOnYourPlanLink();
		
		Thread.sleep(2000);
		addNumber.selectUserSilverPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		userPage.clickOnUsersLink();
		String actualTitleA = userPage.ValidatePageTitle();
		String expectedTitleA = "Users | Callhippo.com";
		assertEquals(actualTitleA, expectedTitleA);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitleB = usUser.ValidatePageTitle();
		String expectedUserSettingTitleB = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitleB, expectedUserSettingTitleB);
		
		usAvailability.clickOnCustomButton();
		
		String actualValidationMessage = usAvailability.validationMessage();
		String expectedValidationMessage = "User updated successfully";
		assertEquals(actualValidationMessage, expectedValidationMessage);
		usAvailability.closeValidationMessage();
		usAvailability.clickOnCustomTimeLink();
		String actualCustomAvailabilityTitle =usAvailability.validateCustomAvailabilityPage();
		String expectedCustomAvailabilityTitle = "Users - Custom Availability | Callhippo.com";
		assertEquals(actualCustomAvailabilityTitle, expectedCustomAvailabilityTitle);
		
		}
	
	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_Custom_button_in_Platinum_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);
		
		usAvailability.clickOnCustomButton();
		assertEquals(usAvailability.selectPlanPopupIsDisplay(), true);
		usAvailability.clickOnSelectPlanButton();
		
		usAvailability.clickOnYourPlanLink();
		
		Thread.sleep(2000);
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$40.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		userPage.clickOnUsersLink();
		String actualTitleA = userPage.ValidatePageTitle();
		String expectedTitleA = "Users | Callhippo.com";
		assertEquals(actualTitleA, expectedTitleA);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitleB = usUser.ValidatePageTitle();
		String expectedUserSettingTitleB = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitleB, expectedUserSettingTitleB);
		
		usAvailability.clickOnCustomButton();
		
		String actualValidationMessage = usAvailability.validationMessage();
		String expectedValidationMessage = "User updated successfully";
		assertEquals(actualValidationMessage, expectedValidationMessage);
		usAvailability.closeValidationMessage();
		usAvailability.clickOnCustomTimeLink();
		String actualCustomAvailabilityTitle =usAvailability.validateCustomAvailabilityPage();
		String expectedCustomAvailabilityTitle = "Users - Custom Availability | Callhippo.com";
		assertEquals(actualCustomAvailabilityTitle, expectedCustomAvailabilityTitle);
		
		}
	
}
