package testCases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import pages.AddNumber;
import pages.LoginPage;
import pages.NumberSettingPage;
import pages.RegistrationPage;
import pages.USUser;
import pages.UsersPage;
import utilsFile.PropertiesFile;
import utilsFile.Retry;
import utilsFile.Utilitylib;

public class USUserTest {

	USUser usUser;
	UsersPage userPage;
	NumberSettingPage numberSettingPage;
	AddNumber addNumber;
	LoginPage loginPage;
	WebDriver driver;
	static Utilitylib excel;
	RegistrationPage registrationPage;
	PropertiesFile url;

	public USUserTest() throws Exception {
		url = new PropertiesFile();
		excel = new Utilitylib("..\\Web\\src\\main\\java\\config\\Signup.xlsx");
	}

	@BeforeMethod
	public void initialization() throws IOException {

		driver = TestBase.init();

		usUser = PageFactory.initElements(driver, USUser.class);
		userPage = PageFactory.initElements(driver, UsersPage.class);
		numberSettingPage = PageFactory.initElements(driver, NumberSettingPage.class);
		addNumber = PageFactory.initElements(driver, AddNumber.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = result.getTestContext().getName()+" - Fail";
			Utilitylib.Screenshot(driver, testname,"Fail "+result.getMethod().getMethodName());
		} else {
			String testname = result.getTestContext().getName()+" - Pass";
			Utilitylib.Screenshot(driver, testname,"Pass "+result.getMethod().getMethodName());
		}
		driver.quit();
	}

	public String signupAndSignin() throws Exception {
		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		driver.get(url.signUp());
		registrationPage.enterFullname("tstAutomation");
		registrationPage.enterCompanyName("appitSimple");
		registrationPage.enterMobile("918511695975");
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("12345678");
		registrationPage.clickOnSignupButton();
		boolean actualResultr = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResultr = true;
		assertEquals(actualResultr, expectedResultr);

		registrationPage.ValidateVerifyButtonOnGmail();
//		boolean actualVerifyMsg = registrationPage.accountVerifiedMsg();
//		boolean expectedVerifyMsg = true;
//		assertEquals(actualVerifyMsg, expectedVerifyMsg);
//
//		driver.get(url.signIn());
//		loginPage.enterEmail(email);
//		loginPage.enterPassword("12345678");
//		loginPage.clickOnLogin();

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.closePopup();
		addNumber.closePopup();

		return email;
	}

	public void addNumberAndOpenTheirNumberSettingPage() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();

		String numberBeforePurchased = addNumber.getSeletedNumber();
		addNumber.selectNumber();
		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();
		addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$8.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		String NumberAfterPurchased = addNumber.getNumeberAfterPurchased();
		assertEquals(numberBeforePurchased, NumberAfterPurchased);

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);
	}

	public void addNumberWithSilverPlan() {
		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber();

		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();

		addNumber.selectMontly();
		addNumber.selectSilver();
		addNumber.clickOnCheckoutButton();

		// addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		// numberSettingPage.invisiblityOfValidationMessage();

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);

	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_User_Setting_page_is_open_successfully() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_UserName_displayed_in_userSetting_Page() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		String actualName = usUser.getFullName();
		String expectedName = "tstAutomation";
		assertEquals(actualName, expectedName);

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_edit_functionality_of_fullName() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usUser.clickOnEditNameIcon();
		usUser.clearFullNameTextbox();
		usUser.enterFullName("Test Automation");
		usUser.clickOnSaveButtonForFullName();

		String actualName = usUser.getFullName();
		String expectedName = "Test Automation";
		assertEquals(actualName, expectedName);

		usUser.clickOnEditNameIcon();
		usUser.clearFullNameTextbox();
		String actualValidationMsg = usUser.validateRequiredFieldValidationMessage();
		String expectedValidationMsg = "Full Name is required";
		assertEquals(actualValidationMsg, expectedValidationMsg);

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_EmailAddress_displayed_in_userSetting_Page() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		String actualName = usUser.getEmailAddress();
		String expectedName = email;
		assertEquals(actualName, expectedName);

	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_Extension_Field_Validation_While_running_Silver_Plan() throws Exception {
		String email = signupAndSignin();

		addNumberWithSilverPlan();

		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		String actualName = usUser.getEmailAddress();
		String expectedName = email;
		assertEquals(actualName, expectedName);

		usUser.clickOnEditExtensionIcon();
		usUser.clearExtensionTextbox();
		String actualRequireFieldValidationMsg = usUser.validateRequireFieldValidationMessageForExtension();
		String expectedRequireFieldValidationMsg = "User Extension is required";
		assertEquals(actualRequireFieldValidationMsg, expectedRequireFieldValidationMsg);

		usUser.enterExtensionTextbox("11");
		String actualLessThan3DigitValidationMessage = usUser.validateLessThan3DigitValidation();
		String expectedLessThan3DigitValidationMessage = "User Extension cannot be less than 3 digits.";
		assertEquals(actualLessThan3DigitValidationMessage, expectedLessThan3DigitValidationMessage);

		usUser.clearExtensionTextbox();
		usUser.enterExtensionTextbox("1112");
		String actualMoreThan3DigitValidationMessage = usUser.validateMoreThan3DigitValidation();
		String expectedMoreThan3DigitValidationMessage = "User Extension cannot be more than 3 digits.";
		assertEquals(actualMoreThan3DigitValidationMessage, expectedMoreThan3DigitValidationMessage);

		usUser.clearExtensionTextbox();
		usUser.enterExtensionTextbox("101");
		usUser.clickOnSaveButtonForExtension();
		String actualUpdateMsg = usUser.validateUserUpdatedValidationMessage();
		String expectedUpdateMsg = "User updated successfully";
		assertEquals(actualUpdateMsg, expectedUpdateMsg);

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_edit_Extension_Field_while_Runing_Bronze_plan_Validation() throws Exception {
		String email = signupAndSignin();

		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber();

		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();

		addNumber.selectMontly();
		addNumber.selectBronze();
		addNumber.clickOnCheckoutButton();

		// addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		// numberSettingPage.invisiblityOfValidationMessage();

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);

		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle1 = userPage.ValidatePageTitle();
		String expectedTitle1 = "Users | Callhippo.com";
		assertEquals(actualTitle1, expectedTitle1);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		String actualName = usUser.getEmailAddress();
		String expectedName = email;
		assertEquals(actualName, expectedName);

		usUser.clickOnEditExtensionIcon();
		usUser.clearExtensionTextbox();
//		usUser.enterExtensionTextbox("101");
//		usUser.clickOnSaveButtonForExtension();
		String actualUpdateMsg = usUser.validateUserUpdatedValidationErrorMessage();
		String expectedUpdateMsg = "You can not edit user extension in bronze Plan. Please Upgrade your plan.";
		assertEquals(actualUpdateMsg, expectedUpdateMsg);

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_edit_Extension_Field_while_Runing_Basic_plan_Validation() throws Exception {
		String email = signupAndSignin();

		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber();

		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();

//		addNumber.selectMontly();
//		addNumber.selectBronze();
//		addNumber.clickOnCheckoutButton();

		addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$8.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		// numberSettingPage.invisiblityOfValidationMessage();

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);

		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle1 = userPage.ValidatePageTitle();
		String expectedTitle1 = "Users | Callhippo.com";
		assertEquals(actualTitle1, expectedTitle1);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		String actualName = usUser.getEmailAddress();
		String expectedName = email;
		assertEquals(actualName, expectedName);

		usUser.clickOnEditExtensionIcon();
		usUser.clearExtensionTextbox();
//		usUser.enterExtensionTextbox("101");
//		usUser.clickOnSaveButtonForExtension();
		String actualUpdateMsg = usUser.validateUserUpdatedValidationErrorMessageForBasic();
		String expectedUpdateMsg = "You can not edit user extension in basic Plan. Please Upgrade your plan.";
		assertEquals(actualUpdateMsg, expectedUpdateMsg);

	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_edit_Extension_Field_while_Runing_Platinum_plan_Validation() throws Exception {
		String email = signupAndSignin();

		addNumber.clickOnNumberLink();
		addNumber.clickOnAddNumberButton();
		addNumber.enterNameofNumber("Number 1");
		addNumber.clickOnUSCountry();
		addNumber.selectNumber();

		addNumber.selectNumberMontlyPlan();
		addNumber.clickOnPayButton();

		addNumber.selectMontly();
		addNumber.selectPlatinum();
		addNumber.clickOnCheckoutButton();

		// addNumber.clickOnSkipLink();

		addNumber.waitforCheckoutPage();

		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$40.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForAddNumberPage();
		addNumber.clickOnNotNowIamDone();
		addNumber.clickOnSaveButton();

		String actualMessage = addNumber.validateNumberSaveSuccessfully();
		String expectedMessage = "Number added successfully";
		assertEquals(actualMessage, expectedMessage);

		// numberSettingPage.invisiblityOfValidationMessage();

		boolean isSettingIconDisplayed = numberSettingPage.validateSettingIcon();
		boolean isDeleteIconDisplayed = numberSettingPage.validateDeleteIcon();

		assertEquals(isSettingIconDisplayed, true);
		assertEquals(isDeleteIconDisplayed, true);

		addNumber.clickOnNumberSetting();

		String actualTitle = numberSettingPage.verifyNumberSettingPage();
		String expectedTitle = "Number Settings | Callhippo.com";

		assertEquals(actualTitle, expectedTitle);

		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle1 = userPage.ValidatePageTitle();
		String expectedTitle1 = "Users | Callhippo.com";
		assertEquals(actualTitle1, expectedTitle1);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		String actualName = usUser.getEmailAddress();
		String expectedName = email;
		assertEquals(actualName, expectedName);

		usUser.clickOnEditExtensionIcon();
		usUser.clearExtensionTextbox();
		String actualRequireFieldValidationMsg = usUser.validateRequireFieldValidationMessageForExtension();
		String expectedRequireFieldValidationMsg = "User Extension is required";
		assertEquals(actualRequireFieldValidationMsg, expectedRequireFieldValidationMsg);

		usUser.enterExtensionTextbox("11");
		String actualLessThan3DigitValidationMessage = usUser.validateLessThan3DigitValidation();
		String expectedLessThan3DigitValidationMessage = "User Extension cannot be less than 3 digits.";
		assertEquals(actualLessThan3DigitValidationMessage, expectedLessThan3DigitValidationMessage);

		usUser.clearExtensionTextbox();
		usUser.enterExtensionTextbox("1112");
		String actualMoreThan3DigitValidationMessage = usUser.validateMoreThan3DigitValidation();
		String expectedMoreThan3DigitValidationMessage = "User Extension cannot be more than 3 digits.";
		assertEquals(actualMoreThan3DigitValidationMessage, expectedMoreThan3DigitValidationMessage);

		usUser.clearExtensionTextbox();
		usUser.enterExtensionTextbox("101");
		usUser.clickOnSaveButtonForExtension();
		String actualUpdateMsg = usUser.validateUserUpdatedValidationMessage();
		String expectedUpdateMsg = "User updated successfully";
		assertEquals(actualUpdateMsg, expectedUpdateMsg);

	}

}
