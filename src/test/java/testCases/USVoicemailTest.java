package testCases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import pages.AddNumber;
import pages.LoginPage;
import pages.NumberSettingPage;
import pages.RegistrationPage;
import pages.USForwordToDevice;
import pages.USUser;
import pages.USVoicemail;
import pages.UsersPage;
import utilsFile.PropertiesFile;
import utilsFile.Retry;
import utilsFile.Utilitylib;

public class USVoicemailTest {

	USVoicemail usVoicemail;
	USUser usUser;
	UsersPage userPage;
	AddNumber addNumber;
	LoginPage loginPage;
	WebDriver driver;
	static Utilitylib excel;
	RegistrationPage registrationPage;
	PropertiesFile url;

	public USVoicemailTest() throws Exception {
		url = new PropertiesFile();
		excel = new Utilitylib("..\\Web\\src\\main\\java\\config\\Signup.xlsx");
	}

	@BeforeMethod
	public void initialization() throws IOException {

		driver = TestBase.init();

		usVoicemail = PageFactory.initElements(driver, USVoicemail.class);
		usUser = PageFactory.initElements(driver, USUser.class);
		userPage = PageFactory.initElements(driver, UsersPage.class);
		addNumber = PageFactory.initElements(driver, AddNumber.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = result.getTestContext().getName()+" - Fail";
			Utilitylib.Screenshot(driver, testname,"Fail "+result.getMethod().getMethodName());
		} else {
			String testname = result.getTestContext().getName()+" - Pass";
			Utilitylib.Screenshot(driver, testname,"Pass "+result.getMethod().getMethodName());
		}
		driver.quit();
	}

	public String signupAndSignin() throws Exception {
		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		driver.get(url.signUp());
		registrationPage.enterFullname("tstAutomation");
		registrationPage.enterCompanyName("appitSimple");
		registrationPage.enterMobile("918511695975");
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("12345678");
		registrationPage.clickOnSignupButton();
		boolean actualResultr = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResultr = true;
		assertEquals(actualResultr, expectedResultr);

		registrationPage.ValidateVerifyButtonOnGmail();
//		boolean actualVerifyMsg = registrationPage.accountVerifiedMsg();
//		boolean expectedVerifyMsg = true;
//		assertEquals(actualVerifyMsg, expectedVerifyMsg);
//
//		driver.get(url.signIn());
//		loginPage.enterEmail(email);
//		loginPage.enterPassword("12345678");
//		loginPage.clickOnLogin();

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.closePopup();
		addNumber.closePopup();

		return email;
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_VoicemailToggle() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage2 = usVoicemail.validationMessage();
		String expectedErrorMessage2 = "User updated successfully";
		assertEquals(actualErrorMessage2, expectedErrorMessage2);
		usVoicemail.closeValidationMessage();
	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_Language_Dropdown_validation() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.selectLanguageFromDropdown("French");
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_voice_Radio_button() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMaleVoiceRadioButton();
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnFemaleVoiceRadioButton();
		String actualErrorMessage2 = usVoicemail.validationMessage();
		String expectedErrorMessage2 = "User updated successfully";
		assertEquals(actualErrorMessage2, expectedErrorMessage2);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_Message_Field_validation() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clearMessageTextbox();
		String actualMessage = usVoicemail.requiredFieldValidationMessage();
		String expectedMessage = "Voice mail is required";
		assertEquals(actualMessage, expectedMessage);

		usVoicemail.enterMessageInTextbox("#@1212Hello");
		usVoicemail.clickOnSaveButton();
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "Special characters are not allowed.";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();
	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_edit_message_in_Basic_Monthly_plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clearMessageTextbox();
		usVoicemail.enterMessageInTextbox("Hello, have a great day.");
		usVoicemail.clickOnSaveButton();
		assertEquals(usVoicemail.selectPlanPopupIsDisplay(), true);

	}

	@Test(priority = 6, retryAnalyzer = Retry.class)
	public void verify_edit_message_in_Bronze_Monthly_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clearMessageTextbox();
		usVoicemail.enterMessageInTextbox("Hello, have a great day.");
		usVoicemail.clickOnSaveButton();
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 7, retryAnalyzer = Retry.class)
	public void verify_edit_message_in_Bronze_Annually_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$96.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clearMessageTextbox();
		usVoicemail.enterMessageInTextbox("Hello, have a great day.");
		usVoicemail.clickOnSaveButton();
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 8, retryAnalyzer = Retry.class)
	public void verify_edit_message_in_Silver_Monthly_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserSilverPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clearMessageTextbox();
		usVoicemail.enterMessageInTextbox("Hello, have a great day.");
		usVoicemail.clickOnSaveButton();
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 9, retryAnalyzer = Retry.class)
	public void verify_edit_message_in_Silver_Annually_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserSilverPlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$180.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clearMessageTextbox();
		usVoicemail.enterMessageInTextbox("Hello, have a great day.");
		usVoicemail.clickOnSaveButton();
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 10, retryAnalyzer = Retry.class)
	public void verify_edit_message_in_Platinum_Monthly_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserMontlyPlan();
		;
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$40.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clearMessageTextbox();
		usVoicemail.enterMessageInTextbox("Hello, have a great day.");
		usVoicemail.clickOnSaveButton();
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 11, retryAnalyzer = Retry.class)
	public void verify_edit_message_in_Platinum_Annually_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$420.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clearMessageTextbox();
		usVoicemail.enterMessageInTextbox("Hello, have a great day.");
		usVoicemail.clickOnSaveButton();
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 12, retryAnalyzer = Retry.class)
	public void verify_upload_less_than_5MB_mp3_music_file() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\MP3_5MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "Your file has been uploaded successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 13, retryAnalyzer = Retry.class)
	public void verify_upload_less_than_5MB_WAV_music_file() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\WAV_4_98MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "Your file has been uploaded successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 14, retryAnalyzer = Retry.class)
	public void verify_upload_more_than_5MB_mp3_music_file() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\MP3_10MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.ValidationText();
		String expectedErrorMessage1 = "File is too large, max size is 5MB.";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);

	}

	@Test(priority = 15, retryAnalyzer = Retry.class)
	public void verify_upload_more_than_5MB_WAV_music_file() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\WAV_10MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.ValidationText();
		String expectedErrorMessage1 = "File is too large, max size is 5MB.";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
	}

	@Test(priority = 16, retryAnalyzer = Retry.class)
	public void verify_upload_mp3_music_file_while_basic_plan_is_running() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\MP3_5MB.exe");
		Thread.sleep(2000);
		assertEquals(usVoicemail.selectPlanPopupIsDisplay(), true);

	}

	@Test(priority = 17, retryAnalyzer = Retry.class)
	public void verify_upload_WAV_music_file_while_basic_plan_is_running() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\WAV_4_98MB.exe");
		Thread.sleep(2000);
		assertEquals(usVoicemail.selectPlanPopupIsDisplay(), true);

	}

	@Test(priority = 18, retryAnalyzer = Retry.class)
	public void verify_upload_music_file_while_basic_plan_is_running_UpgradePlan_through_select_Plan_popup()
			throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\WAV_4_98MB.exe");
		Thread.sleep(2000);
		assertEquals(usVoicemail.selectPlanPopupIsDisplay(), true);

		usVoicemail.clickOnSelectPlanButton(); 

		usVoicemail.clickOnYourPlanLink();
		Thread.sleep(2000);
		addNumber.selectUserBronzePlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$10.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);

		userPage.clickOnUsersLink();
		String actualTitleA = userPage.ValidatePageTitle();
		String expectedTitleA = "Users | Callhippo.com";
		assertEquals(actualTitleA, expectedTitleA);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitleB = usUser.ValidatePageTitle();
		String expectedUserSettingTitleB = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitleB, expectedUserSettingTitleB);

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\WAV_4_98MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "Your file has been uploaded successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 19, retryAnalyzer = Retry.class)
	public void verify_upload_less_than_5MB_mp3_music_file_in_Silver_monthly_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserSilverPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$18.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\MP3_5MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "Your file has been uploaded successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 20, retryAnalyzer = Retry.class)
	public void verify_upload_less_than_5MB_mp3_music_file_in_Silver_Annually_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserSilverPlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$180.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\MP3_5MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "Your file has been uploaded successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 21, retryAnalyzer = Retry.class)
	public void verify_upload_less_than_5MB_mp3_music_file_in_Platinum_monthly_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserMontlyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$40.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\MP3_5MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "Your file has been uploaded successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

	@Test(priority = 22, retryAnalyzer = Retry.class)
	public void verify_upload_less_than_5MB_mp3_music_file_in_Platinum_Annually_Plan() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		addNumber.clickOnSetting();
		addNumber.selectUserPlatinumPlan();
		addNumber.selectUserAnuallyPlan();
		addNumber.clickOnUpgradeButton();
		addNumber.waitforCheckoutPage();
		String actualPrice = addNumber.getTotalPrice();
		String expectedPrice = "$420.00";
		assertEquals(actualPrice, expectedPrice);

		addNumber.filledCheckoutPage();
		addNumber.clickOnSubscripbeButton();

		addNumber.waitForSubscribePage();

		String actualSuccessfullyMsg = addNumber.validateUpdradeSuccessfullyMessage();
		String expectedSuccessfullyMsg = "Your plan has been upgraded successfully.";
		assertEquals(actualSuccessfullyMsg, expectedSuccessfullyMsg);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usVoicemail.clickOnVoicemailToggle();
		String actualErrorMessage = usVoicemail.validationMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usVoicemail.closeValidationMessage();

		usVoicemail.clickOnMusicRadioButton();
		usVoicemail.clickOnchangeFileButton();
		Thread.sleep(2000);
		Runtime.getRuntime().exec("..\\Web\\src\\main\\java\\audioFiles\\MP3_5MB.exe");
		Thread.sleep(2000);
		String actualErrorMessage1 = usVoicemail.validationMessage();
		String expectedErrorMessage1 = "Your file has been uploaded successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usVoicemail.closeValidationMessage();

	}

}
