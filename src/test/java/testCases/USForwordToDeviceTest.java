package testCases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.TestBase;
import pages.AddNumber;
import pages.LoginPage;
import pages.NumberSettingPage;
import pages.RegistrationPage;
import pages.USForwordToDevice;
import pages.USUser;
import pages.UsersPage;
import utilsFile.PropertiesFile;
import utilsFile.Retry;
import utilsFile.Utilitylib;

public class USForwordToDeviceTest {

	USForwordToDevice usForwordToDevice;
	USUser usUser;
	UsersPage userPage;
	NumberSettingPage numberSettingPage;
	AddNumber addNumber;
	LoginPage loginPage;
	WebDriver driver;
	static Utilitylib excel;
	RegistrationPage registrationPage;
	PropertiesFile url;

	public USForwordToDeviceTest() throws Exception {
		url = new PropertiesFile();
		excel = new Utilitylib("..\\Web\\src\\main\\java\\config\\Signup.xlsx");
	}

	@BeforeMethod
	public void initialization() throws IOException {

		driver = TestBase.init();

		usForwordToDevice = PageFactory.initElements(driver, USForwordToDevice.class);
		usUser = PageFactory.initElements(driver, USUser.class);
		userPage = PageFactory.initElements(driver, UsersPage.class);
		numberSettingPage = PageFactory.initElements(driver, NumberSettingPage.class);
		addNumber = PageFactory.initElements(driver, AddNumber.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		registrationPage = PageFactory.initElements(driver, RegistrationPage.class);
	}

	@AfterMethod
	public void tearDown(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {
			String testname = result.getTestContext().getName()+" - Fail";
			Utilitylib.Screenshot(driver, testname,"Fail "+result.getMethod().getMethodName());
		} else {
			String testname = result.getTestContext().getName()+" - Pass";
			Utilitylib.Screenshot(driver, testname,"Pass "+result.getMethod().getMethodName());
		}
		driver.quit();
	}

	public String signupAndSignin() throws Exception {
		String date = excel.date();
		String email = "jayadip+" + date + "@callhippo.com";

		driver.get(url.signUp());
		registrationPage.enterFullname("tstAutomation");
		registrationPage.enterCompanyName("appitSimple");
		registrationPage.enterMobile("918511695975");
		registrationPage.enterEmail(email);
		registrationPage.enterPassword("12345678");
		registrationPage.clickOnSignupButton();
		boolean actualResultr = registrationPage.nonRegisteredEmailValidation();
		boolean expectedResultr = true;
		assertEquals(actualResultr, expectedResultr);

		registrationPage.ValidateVerifyButtonOnGmail();
//		boolean actualVerifyMsg = registrationPage.accountVerifiedMsg();
//		boolean expectedVerifyMsg = true;
//		assertEquals(actualVerifyMsg, expectedVerifyMsg);
//
//		driver.get(url.signIn());
//		loginPage.enterEmail(email);
//		loginPage.enterPassword("12345678");
//		loginPage.clickOnLogin();

		String actualResultl = loginPage.loginSuccessfully();
		String expectedResultl = "Dashboard | Callhippo.com";
		assertEquals(actualResultl, expectedResultl);

		addNumber.closePopup();
		addNumber.closePopup();

		return email;
	}

	@Test(priority = 1, retryAnalyzer = Retry.class)
	public void verify_Forword_To_Device_Toggle() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usForwordToDevice.clickOnForwordToDeviceToggle();
		usForwordToDevice.waitForValidationMessage();
		String actualValidation = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedValidation = "User updated successfully";
		assertEquals(actualValidation, expectedValidation);
		usForwordToDevice.closeValidationMessagePopup();

		usForwordToDevice.clickOnForwordToDeviceToggle();
		usForwordToDevice.waitForValidationMessage();
		String actualValidation1 = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedValidation1 = "User updated successfully";
		assertEquals(actualValidation1, expectedValidation1);

	}

	@Test(priority = 2, retryAnalyzer = Retry.class)
	public void verify_edit_number_validation() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usForwordToDevice.ClickOnEditNumberButton();
		usForwordToDevice.clearNumberTextbox();
		usForwordToDevice.ClickOnSaveButton();
		usForwordToDevice.waitForErrorValidationMessage();
		String actualErrorMessage = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage = "Please Enter Number With + and CountryCode.";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usForwordToDevice.closeValidationMessagePopup();

		usForwordToDevice.enterNumberInTextbox("9998503333");
		usForwordToDevice.ClickOnSaveButton();
		usForwordToDevice.waitForErrorValidationMessage();
		String actualErrorMessage1 = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage1 = "Please Enter Number With + and CountryCode.";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usForwordToDevice.closeValidationMessagePopup();

		usForwordToDevice.clearNumberTextbox();
		usForwordToDevice.enterNumberInTextbox("+9898");
		usForwordToDevice.ClickOnSaveButton();
		String actualErrorMessage2 = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage2 = "Please Enter Valid Number.";
		assertEquals(actualErrorMessage2, expectedErrorMessage2);
		usForwordToDevice.closeValidationMessagePopup();

		usForwordToDevice.clearNumberTextbox();
		usForwordToDevice.enterNumberInTextbox("+9998503333");
		usForwordToDevice.ClickOnSaveButton();
		usForwordToDevice.waitForValidationMessage();
		String actualErrorMessage3 = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage3 = "User updated successfully";
		assertEquals(actualErrorMessage3, expectedErrorMessage3);
		usForwordToDevice.closeValidationMessagePopup();

	}

	@Test(priority = 3, retryAnalyzer = Retry.class)
	public void verify_with_add_multiple_numbers() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usForwordToDevice.ClickOnEditNumberButton();
		usForwordToDevice.clearNumberTextbox();
		usForwordToDevice.enterNumberInTextbox("+9998603368");
		usForwordToDevice.ClickOnSaveButton();
		String actualErrorMessage = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usForwordToDevice.closeValidationMessagePopup();

		usForwordToDevice.ClickOnAddButton();
		usForwordToDevice.enterNumberInTextbox2("+9998603398");
		usForwordToDevice.ClickOnSaveButton();
		String actualErrorMessage1 = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usForwordToDevice.closeValidationMessagePopup();

	}

	@Test(priority = 4, retryAnalyzer = Retry.class)
	public void verify_with_add_duplicate_numbers() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usForwordToDevice.ClickOnEditNumberButton();
		usForwordToDevice.clearNumberTextbox();
		usForwordToDevice.enterNumberInTextbox("+9998603368");
		usForwordToDevice.ClickOnSaveButton();
		String actualErrorMessage = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usForwordToDevice.closeValidationMessagePopup();

		usForwordToDevice.ClickOnAddButton();
		usForwordToDevice.enterNumberInTextbox2("+9998603368");
		usForwordToDevice.ClickOnSaveButton();
		String actualErrorMessage1 = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage1 = "A number is already present.";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usForwordToDevice.closeValidationMessagePopup();

	}

	@Test(priority = 5, retryAnalyzer = Retry.class)
	public void verify_with_delete_number() throws Exception {
		String email = signupAndSignin();
		Thread.sleep(2000);
		userPage.clickOnUsersLink();
		String actualTitle = userPage.ValidatePageTitle();
		String expectedTitle = "Users | Callhippo.com";
		assertEquals(actualTitle, expectedTitle);

		usUser.clickOnSettingIconOf(email);
		String actualUserSettingTitle = usUser.ValidatePageTitle();
		String expectedUserSettingTitle = "User Settings | Callhippo.com";
		assertEquals(actualUserSettingTitle, expectedUserSettingTitle);

		usForwordToDevice.ClickOnEditNumberButton();
		usForwordToDevice.clearNumberTextbox();
		usForwordToDevice.enterNumberInTextbox("+9998603368");
		usForwordToDevice.ClickOnSaveButton();
		String actualErrorMessage = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage = "User updated successfully";
		assertEquals(actualErrorMessage, expectedErrorMessage);
		usForwordToDevice.closeValidationMessagePopup();

		usForwordToDevice.ClickOnAddButton();
		usForwordToDevice.enterNumberInTextbox2("+9998603398");
		usForwordToDevice.ClickOnSaveButton();
		String actualErrorMessage1 = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage1 = "User updated successfully";
		assertEquals(actualErrorMessage1, expectedErrorMessage1);
		usForwordToDevice.closeValidationMessagePopup();

		usForwordToDevice.clickOn1stDeletenumber();
		String actualErrorMessage2 = usForwordToDevice.ValidateUserUpdateMessage();
		String expectedErrorMessage2 = "User updated successfully";
		assertEquals(actualErrorMessage2, expectedErrorMessage2);
		usForwordToDevice.closeValidationMessagePopup();

	}

}
