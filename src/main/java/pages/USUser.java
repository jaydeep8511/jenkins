package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class USUser {
	
	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@FindBy(xpath = "(//div[@class='deparmentname_title']//span[contains(@class,'ng-binding ng-scope')])[1]")WebElement fullName;
	@FindBy(xpath = "(//button[@class='circlest btn btn-sm btn-info'])[1]")WebElement editNameIcon;
	@FindBy(xpath = "//input[@id='fullName']") WebElement fullnameTextbox;
	@FindBy(xpath = "//span[contains(.,'Full Name is required')]")WebElement requiredValidationMessage;
	@FindBy(xpath = "(//i[contains(@class,'fa fa-floppy-o')])[1]")WebElement saveButtonForName;
	@FindBy(xpath = "//span[@class='emailaddress_weight ng-binding']")WebElement email;
	
	@FindBy(xpath = "(//button[@class='circlest btn btn-sm btn-info'])[2]")WebElement editExtensionIcon;
	@FindBy(xpath = "//input[@id='extensionNumber']")WebElement extensionTextbox;
	@FindBy(xpath = "//span[contains(.,'User Extension is required')]")WebElement requireValidationOfExtension;
	@FindBy(xpath = "(//i[@class='fa fa-floppy-o'])[1]")WebElement saveButtonForExtension;
	@FindBy(xpath = "//span[contains(.,'User Extension cannot be less than 3 digits.')]")WebElement lessThan3DigitError;
	@FindBy(xpath = "//span[contains(.,'User Extension cannot be more than 3 digits.')]")WebElement moreThan3DigitError;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'User updated successfully')]")WebElement userUpdatedMsg;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'You can not edit user extension in bronze Plan. Please Upgrade your plan.')]")WebElement userUpdatedErrorMsg;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'You can not edit user extension in basic Plan. Please Upgrade your plan.')]")WebElement userUpdatedErrorMsgofBasic;
	
	public USUser(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}
	
	public String ValidatePageTitle() {
		wait.until(ExpectedConditions.titleContains("User Settings | Callhippo.com"));
		return driver.getTitle();
	}
	
	public void clickOnSettingIconOf(String UserEmail) {
		String path = "//a[contains(text(),'"+UserEmail+"')]/parent::td//following-sibling::td//button[@class='circlest btn btn-sm btn-info']";
		driver.findElement(By.xpath(path));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		 driver.findElement(By.xpath(path)).click();
	}
	
	public String getFullName() {
		wait.until(ExpectedConditions.visibilityOf(fullName));
		return fullName.getText();
	}
	
	public void clickOnEditNameIcon() {
		wait.until(ExpectedConditions.visibilityOf(editNameIcon));
		editNameIcon.click();
	}
	
	public void clearFullNameTextbox() {
		wait.until(ExpectedConditions.visibilityOf(fullnameTextbox));
		fullnameTextbox.clear();
	}
	
	public void enterFullName(String fullname) {
		wait.until(ExpectedConditions.visibilityOf(fullnameTextbox));
		fullnameTextbox.sendKeys(fullname);
	}
	
	public String validateRequiredFieldValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(requiredValidationMessage));
		return requiredValidationMessage.getText();
	}
	
	public void clickOnSaveButtonForFullName() {
		wait.until(ExpectedConditions.visibilityOf(saveButtonForName));
		saveButtonForName.click();
	}
	
	public String getEmailAddress() {
		wait.until(ExpectedConditions.visibilityOf(email));
		return email.getText();
	}
	
	public void clickOnEditExtensionIcon() {
		wait.until(ExpectedConditions.visibilityOf(editExtensionIcon));
		editExtensionIcon.click();
	}
	
	public void clearExtensionTextbox() {
		wait.until(ExpectedConditions.visibilityOf(extensionTextbox));
		extensionTextbox.clear();
	}
	
	public void enterExtensionTextbox(String digit) {
		wait.until(ExpectedConditions.visibilityOf(extensionTextbox));
		extensionTextbox.sendKeys(digit);
	}
	
	public String validateRequireFieldValidationMessageForExtension() {
		wait.until(ExpectedConditions.visibilityOf(requireValidationOfExtension));
		return requireValidationOfExtension.getText();
	}
	
	public void clickOnSaveButtonForExtension() {
		wait.until(ExpectedConditions.visibilityOf(saveButtonForExtension));
		saveButtonForExtension.click();
	}
	
	public String validateMoreThan3DigitValidation() {
		wait.until(ExpectedConditions.visibilityOf(moreThan3DigitError));
		return moreThan3DigitError.getText();
	}
	
	public String validateLessThan3DigitValidation() {
		wait.until(ExpectedConditions.visibilityOf(lessThan3DigitError));
		return lessThan3DigitError.getText();
	}
	
	public String validateUserUpdatedValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(userUpdatedMsg));
		return userUpdatedMsg.getText();
	}
	
	public String validateUserUpdatedValidationErrorMessage() {
		wait.until(ExpectedConditions.visibilityOf(userUpdatedErrorMsg));
		return userUpdatedErrorMsg.getText();
	}
	
	public String validateUserUpdatedValidationErrorMessageForBasic() {
		wait.until(ExpectedConditions.visibilityOf(userUpdatedErrorMsgofBasic));
		return userUpdatedErrorMsgofBasic.getText();
	}
	
}
