package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class USAvailability {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@FindBy(xpath = "//button[@type='button'][contains(.,'×')]")
	WebElement CloseValidationPopup;
	@FindBy(xpath = "//span[@ng-bind-html='message.content']")
	WebElement validationMessage;

	@FindBy(xpath = "//div[@id='availability']//i[@class='fa fa-pencil']")
	WebElement editTimeZone;
	@FindBy(xpath = "//div[@id='availability']//select[@ng-model='vm.module.timezone']")
	WebElement timeZoneDropdown;
	@FindBy(xpath = "//div[@id='availability']//i[@class='fa fa-floppy-o']/parent::a")
	WebElement saveButton;
	@FindBy(xpath = "//div[@id='availability']//div[@ng-bind-html='vm.getTimezoneName(vm.module.timezone);']")
	WebElement getTimeZone;
	@FindBy(xpath = "//div[@id='availability']//a[contains(.,'Always Closed')]")
	WebElement alwaysClosedButton;
	@FindBy(xpath = "//div[@id='availability']//a[contains(.,'Always Opened')]")
	WebElement alwaysOpenedButton;
	@FindBy(xpath = "//div[@id='availability']//a[contains(.,'Custom')]")WebElement customButton;
	@FindBy(xpath = "//div[@id='availability']//a[@class='linkButton']")WebElement customeTimeLink;
	
	@FindBy(xpath = "//button[@class='upgraderecordbtn'][contains(.,'Select Plan')]")WebElement selectPlanButton;
	@FindBy(xpath = "//a[@scroll-on-click='yourplan']") WebElement yourPlan;
	
	public USAvailability(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));
	}

	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}

	public void closeValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(CloseValidationPopup));
		CloseValidationPopup.click();
	}

	public void clickOnEditTimeZone() {
		wait.until(ExpectedConditions.visibilityOf(editTimeZone));
		editTimeZone.click();
	}

	public String selectTimeZoneOf(String visibleText) {
		wait.until(ExpectedConditions.visibilityOf(timeZoneDropdown));
		Select timezone = new Select(timeZoneDropdown);
		timezone.selectByVisibleText(visibleText);
		return visibleText;

	}

	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton));
		saveButton.click();
	}

	public String getTimezoneText() {
		wait.until(ExpectedConditions.visibilityOf(getTimeZone));
		return getTimeZone.getText();
	}

	public void clickOnAlwaysClosedButton() {
		wait.until(ExpectedConditions.visibilityOf(alwaysClosedButton));
		alwaysClosedButton.click();
	}

	public void clickOnAlwaysOpenedButton() {
		wait.until(ExpectedConditions.visibilityOf(alwaysOpenedButton));
		alwaysOpenedButton.click();
	}
	
	public void clickOnCustomButton() {
		wait.until(ExpectedConditions.visibilityOf(customButton));
		customButton.click();
	}
	
	public boolean selectPlanPopupIsDisplay() {
		wait.until(ExpectedConditions.visibilityOf(selectPlanButton));
		return selectPlanButton.isDisplayed();
	}
	
	public void clickOnSelectPlanButton() {
		wait.until(ExpectedConditions.visibilityOf(selectPlanButton));
		selectPlanButton.click();
	}
	
	public void clickOnYourPlanLink() {
		wait.until(ExpectedConditions.visibilityOf(yourPlan));
		yourPlan.click();
	}
	
	public void clickOnCustomTimeLink() {
		wait.until(ExpectedConditions.visibilityOf(customeTimeLink));
		customeTimeLink.click();
	}
	
	public String validateCustomAvailabilityPage() {
		wait.until(ExpectedConditions.titleContains("Users - Custom Availability | Callhippo.com"));
		return driver.getTitle();
	}
}
