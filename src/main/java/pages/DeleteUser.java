package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeleteUser {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@FindBy(xpath = "//button[@type='button'][contains(.,'×')]")
	WebElement CloseValidationPopup;
	@FindBy(xpath = "//span[@ng-bind-html='message.content']")
	WebElement validationMessage;
	
	@FindBy(xpath = "//p[contains(@class,'ng-binding')]") WebElement popupTexts;
	@FindBy(xpath = "//button[contains(.,'Yes')]") WebElement yesButton;
	@FindBy(xpath = "//button[contains(.,'No')]") WebElement noButton;

	public DeleteUser(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));
	}

	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}

	public void closeValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(CloseValidationPopup));
		CloseValidationPopup.click();
	}

	public void clickOnDeleteUserOf(String email) {
		String path = "//a[contains(text(),'" + email
				+ "')]/parent::td//following-sibling::td//div/button[@class='circledelete btn btn-sm btn-danger']";
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		WebElement el = driver.findElement(By.xpath(path));
		el.click();

	}
	
	public String Validate_popup_texts() {
		wait.until(ExpectedConditions.visibilityOf(popupTexts));
		return popupTexts.getText();
	}
	
	public void clickOnYesButton() {
		wait.until(ExpectedConditions.visibilityOf(yesButton));
		yesButton.click();
	}
	
	public void clickOnNoButton() {
		wait.until(ExpectedConditions.visibilityOf(noButton));
		noButton.click();
	}
}
