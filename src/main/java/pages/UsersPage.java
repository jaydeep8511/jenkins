package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UsersPage {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@FindBy(xpath = "//a[contains(.,'supervisor_account Users')]")
	WebElement users;
	@FindBy(xpath = "(//a[@ng-if='userObj.userActive'])[1]")
	WebElement mainUserEmail;
	@FindBy(xpath = "(//a[@ng-if='userObj.userActive'])[6]")
	WebElement subUserEmail;
	@FindBy(xpath = "(//button[@class='circlest btn btn-sm btn-info'])[1]")
	WebElement settingButton;
	@FindBy(xpath = "(//button[@class='circledelete btn btn-sm btn-danger'])[1]")
	WebElement deleteIcon;
	@FindBy(xpath = "//button[@class='greybordrednt waves-effect waves-light btn actnbtnwidnch_btn'][contains(.,'Yes')]")
	WebElement yesButtonOfDeletePopup;
	@FindBy(xpath = "//a[@class='dropdown-toggle userdropdown']")
	WebElement accountDropdown;
	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	WebElement logoutLink;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'User deleted successfully')]")
	WebElement deleteUserMsg;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Your account has been inactive. Please contact us at support@callhippo.com')]")
	WebElement deletedvalidationMsg;

	public UsersPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}

	public String ValidatePageTitle() {
		wait.until(ExpectedConditions.titleContains("Users | Callhippo.com"));
		return driver.getTitle();
	}

	public void clickOnUsersLink() {
		wait.until(ExpectedConditions.visibilityOf(users));
		users.click();
	}

	public String validateMainUserOnUserPage() {
		wait.until(ExpectedConditions.visibilityOf(mainUserEmail));
		return mainUserEmail.getText();
	}

	public String validateSubUserOnUserPage() {
		wait.until(ExpectedConditions.visibilityOf(subUserEmail));
		return subUserEmail.getText();
	}

	public String validateHeaderText(String headerSequence) {
		String headerPath = "//th[" + headerSequence + "]";
		return driver.findElement(By.xpath(headerPath)).getText();
	}

	public int validateDeleteButtonSize() {
		return driver.findElements(By.xpath("//button[@class='circledelete btn btn-sm btn-danger']")).size();
	}

	public int validateSettingButtonSize() {
		return driver.findElements(By.xpath("//button[@class='circlest btn btn-sm btn-info']")).size();
	}

	public void clickOnSettingIcon() {
		wait.until(ExpectedConditions.visibilityOf(settingButton));
		settingButton.click();
	}

	public void clickOnDeleteIcon() {
		wait.until(ExpectedConditions.visibilityOf(deleteIcon));
		deleteIcon.click();
	}

	public String ValidateUserSettingPageTitle() {
		wait.until(ExpectedConditions.titleContains("User Settings | Callhippo.com"));
		return driver.getTitle();
	}

	public void clickOnYesButtonOfDeletePopup() {
		wait.until(ExpectedConditions.visibilityOf(yesButtonOfDeletePopup));
		yesButtonOfDeletePopup.click();
	}

	public void clickOnLogoutLink() {
		wait.until(ExpectedConditions.visibilityOf(accountDropdown));
		accountDropdown.click();

		wait.until(ExpectedConditions.visibilityOf(logoutLink));
		logoutLink.click();

	}

	public String validateDeleteUserValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(deleteUserMsg));
		return deleteUserMsg.getText();
	}

	public String validateloginErrorMessage() {
		wait.until(ExpectedConditions.visibilityOf(deletedvalidationMsg));
		return deletedvalidationMsg.getText();
	}
}
