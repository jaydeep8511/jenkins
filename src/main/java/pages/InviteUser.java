package pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InviteUser {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@FindBy(xpath = "//a[contains(.,'supervisor_account Users')]")
	WebElement users;
	@FindBy(xpath = "//button[text()='Invite User']")
	WebElement inviteUserButton;
	@FindBy(xpath = "//input[@id='email']")
	WebElement email;
	@FindBy(xpath = "//input[@type='checkbox']")
	WebElement numberCheckbox;
	@FindBy(xpath = "//button[@id='inviteBtn']")
	WebElement inviteButton;
	@FindBy(xpath = "//input[@name='fullName']")
	WebElement fullName;
	@FindBy(xpath = "//input[@id='password']")
	WebElement password;
	@FindBy(xpath = "//input[@id='confirm_password']")
	WebElement confirmPassword;
	@FindBy(xpath = "//button[@type='submit']")
	WebElement submitButton;
	@FindBy(xpath = "//b[contains(.,'Your account has been activated.')]")
	WebElement activatedMessage;
	@FindBy(xpath = "//i[@class='material-icons addic new_btn_ic_size_ivr'][contains(.,'save')]")
	WebElement saveButton;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Please enter the Email Id.')]")
	WebElement requiredFieldErrorMsg;
	@FindBy(xpath = "//i[@id='g_reminder_alert_close']")WebElement closeAddCreditPopup;
	
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Before inviting user you must have atleast one number.')]")
	WebElement atleastOneNumeberErrorMsg;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Please enter the valid Email Id.')]")
	WebElement invalidemailValidationMsg;
	@FindBy(xpath = "//i[@class='material-icons addic new_btn_ic_size_ivr'][contains(.,'create')]")
	WebElement editButton;
	@FindBy(xpath = "//i[@class='material-icons addic new_btn_ic_size_ivr'][contains(.,'add')]")
	WebElement addButton;
	@FindBy(xpath = "(//input[@id='email'])[2]")WebElement email2;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Email already exists')]") WebElement emailAlreadyExitErrorMsg;
	@FindBy(xpath = "//button[@type='button'][contains(.,'×')]") WebElement closeValidationPopip;
	@FindBy(xpath = "//span[contains(text(),'An invitation email has been sent')]") WebElement sentInvitationValidationMsg;
	@FindBy(xpath = "//p[@ng-if='options.message']")WebElement exededUserLimitPopupText;
	@FindBy(xpath = "//button[@ng-repeat='button in options.buttons track by button.label'][contains(.,'Yes')]")WebElement yesButton;
	@FindBy(xpath = "//button[@ng-repeat='button in options.buttons track by button.label'][contains(.,'No')]")WebElement noButton;
	@FindBy(xpath = "//div[@class='deparmentname_title']/span[@class='ng-binding ng-scope'][contains(.,'Invited User')]")WebElement subUserName;
	
	@FindBy(xpath = "//span[@class='help-block'][contains(.,'name is required.')]")WebElement nameRequiredMsg;
	@FindBy(xpath = "//span[@class='help-block'][contains(.,'Password is required.')]")WebElement passwordRequiredMsg;
	@FindBy(xpath = "//span[@class='help-block'][contains(.,'Confirm password is required.')]")WebElement confirmPasswordRequiredMsg;
	@FindBy(xpath = "//span[@class='help-block'][contains(.,'name is too short.')]")WebElement nameIsTooShortMsg;
	@FindBy(xpath = "//span[@class='help-block'][contains(.,'Password is too short.')]")WebElement passwordIsTooShortMsg;
	@FindBy(xpath = "//span[@class='help-block'][contains(.,'Confirm password is too short.')]")WebElement confirmPasswordIsTooShortMsg;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Password and confirm password does not matched')]")WebElement mismatchPasswordValidationMsg;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Sorry this invitation request has expired. Please contact your admin user.')]") WebElement expiredInvitationLink;
	@FindBy(xpath = "//a[contains(text(),'Logout')]")WebElement logout;
	
	@FindBy(xpath = "//input[@id='identifierId']")
	WebElement gemail;
	@FindBy(xpath = "(//span[contains(.,'Next')])[2]")
	WebElement gemailNext;
	@FindBy(xpath = "//input[@name='password']")
	WebElement gpassword;
	@FindBy(xpath = "(//span[contains(.,'Next')])[2]")
	WebElement gpasswordNext;
	@FindBy(xpath = "//span[contains(text(),'CallHippo | Confirm Your Email')]")
	WebElement mailtitle;

	public InviteUser(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}

	public void clickOnUsersLink() {
		wait.until(ExpectedConditions.visibilityOf(users));
		users.click();
	}

	public void clickOnInviteUserButton() {
		wait.until(ExpectedConditions.visibilityOf(inviteUserButton));
		inviteUserButton.click();
	}

	public String validateTitle() {

		wait.until(ExpectedConditions.titleContains("Invite User | Callhippo.com"));
		return driver.getTitle();

	}

	public void enterEmail(String email) {
		wait.until(ExpectedConditions.visibilityOf(this.email));
		this.email.sendKeys(email);

	}

	public void clearEmail() {
		wait.until(ExpectedConditions.visibilityOf(email));
		email.clear();

	}

	public void enterEmail2(String email) {
		wait.until(ExpectedConditions.visibilityOf(this.email2));
		this.email2.sendKeys(email);

	}

	public void clearEmail2() {
		wait.until(ExpectedConditions.visibilityOf(email2));
		email2.clear();

	}
	
	public void clickOnNumberCheckbox() {
		wait.until(ExpectedConditions.visibilityOf(numberCheckbox));
		numberCheckbox.click();
	}

	public void clickOnInviteButton() {
		wait.until(ExpectedConditions.elementToBeClickable(inviteButton));
		inviteButton.click();
	}

	public void waitUntilSuccessfulValidationMsgDisplay() {
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(
				By.xpath("//span[@ng-bind-html='message.content'][contains(.,'Your invitation sent successfully')]"))));

	}

	public void enterFullName(String fullName) {
		wait.until(ExpectedConditions.visibilityOf(this.fullName));
		this.fullName.sendKeys(fullName);
	}
	
	public void clearFullName() {
		wait.until(ExpectedConditions.visibilityOf(fullName));
		fullName.clear();
	}

	public void enterPassword(String password) {
		wait.until(ExpectedConditions.visibilityOf(this.password));
		this.password.sendKeys(password);
	}

	public void clearPassword() {
		wait.until(ExpectedConditions.visibilityOf(password));
		password.clear();
	}
	
	public void enterconfirmPassword(String confirmPassword) {
		wait.until(ExpectedConditions.visibilityOf(this.confirmPassword));
		this.confirmPassword.sendKeys(confirmPassword);
	}
	
	public void clearConfirmPassword() {
		wait.until(ExpectedConditions.visibilityOf(confirmPassword));
		confirmPassword.clear();
	}

	public void clickOnSubmitButton() {
		wait.until(ExpectedConditions.elementToBeClickable(submitButton));
		submitButton.click();
	}

	public String validateAccountActivatedMessage() {
		wait.until(ExpectedConditions.visibilityOf(activatedMessage));
		return activatedMessage.getText();
	}

	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton));
		saveButton.click();
	}

	
	
	public void waitForAtleastOneNumberValidation() {
		wait.until(ExpectedConditions.visibilityOf(atleastOneNumeberErrorMsg));
		
	}
	
	public void waitForRequiredFieldValidationMsg() {
		wait.until(ExpectedConditions.visibilityOf(requiredFieldErrorMsg));

	}

	public void waitForinvalidEmailValidationMsg() {
		wait.until(ExpectedConditions.visibilityOf(invalidemailValidationMsg));
	}

	public void clickOnEditButton() {
		wait.until(ExpectedConditions.invisibilityOf(editButton));
		editButton.click();
	}

	public void clickOnAddButton() {
		wait.until(ExpectedConditions.visibilityOf(addButton));
		addButton.click();
	}
	
	public void waitForEmailAlreadyExistValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(emailAlreadyExitErrorMsg));
	}
	
	public void closeValidationPopup() {
		wait.until(ExpectedConditions.visibilityOf(closeValidationPopip));
		closeValidationPopip.click();
	}
	
	public String ValidatePendingStatusOf(String invitedUserEmail) {
		String path = "//span[contains(text(),'"+invitedUserEmail+"')]/parent::td//following-sibling::td/span[contains(.,'Pending')]";
		driver.findElement(By.xpath(path));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		return driver.findElement(By.xpath(path)).getText();
	}
	
	public String ValidateActiveStatusOf(String invitedUserEmail) {
		String path = "//a[contains(text(),'"+invitedUserEmail+"')]/parent::td//following-sibling::td/a[contains(text(),'Active')]";
		driver.findElement(By.xpath(path));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		return driver.findElement(By.xpath(path)).getText();
	}
	
	public void clickOnMessageButton(String invitedUserEmail) {
		String path = "//span[contains(text(),'"+invitedUserEmail+"')]/parent::td//following-sibling::td//button[@class='circlest btn btn-sm btn-info']";
		driver.findElement(By.xpath(path));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		driver.findElement(By.xpath(path)).click();
	
	}
	
	public void clickOnSettingIconOf(String invitedUserEmail) {
		String path = "//a[contains(text(),'"+invitedUserEmail+"')]/parent::td//following-sibling::td//button[@class='circlest btn btn-sm btn-info']";
		driver.findElement(By.xpath(path));
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		 driver.findElement(By.xpath(path)).click();
	}
	
	public void waitForSentInvitationValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(sentInvitationValidationMsg));
	}
	
	public String validateExededUserLimitPopupText() {
		wait.until(ExpectedConditions.visibilityOf(exededUserLimitPopupText));
		return exededUserLimitPopupText.getText();
	}
	
	public void clickOnYesButtonOfExededPopup() {
		wait.until(ExpectedConditions.visibilityOf(yesButton));
		yesButton.click();
	}
	
	public void clickOnNoButtonOfExededPopup() {
		wait.until(ExpectedConditions.visibilityOf(noButton));
		noButton.click();
	}
	
	public String validateInvitedUserName() {
		wait.until(ExpectedConditions.visibilityOf(subUserName));
		return subUserName.getText();
	}
	
	public String validateNameRequireFieldValidation() {
		wait.until(ExpectedConditions.visibilityOf(nameRequiredMsg));
		return nameRequiredMsg.getText();
	}
	
	public String validateNameIsTooShortValidation() {
		wait.until(ExpectedConditions.visibilityOf(nameIsTooShortMsg));
		return nameIsTooShortMsg.getText();
	}
	
	public String validatePasswordRequiredFieldValidation() {
		wait.until(ExpectedConditions.visibilityOf(passwordRequiredMsg));
		return passwordRequiredMsg.getText();
	}

	public String validateConfirmPasswordRequiredFieldValidation() {
		wait.until(ExpectedConditions.visibilityOf(confirmPasswordRequiredMsg));
		return confirmPasswordRequiredMsg.getText();
	}
	
	public String validatePasswordIsTooShortValidation() {
		wait.until(ExpectedConditions.visibilityOf(passwordIsTooShortMsg));
		return passwordIsTooShortMsg.getText();
	}
	
	public String validateConfirmPasswordIsTooShortValidation() {
		wait.until(ExpectedConditions.visibilityOf(confirmPasswordIsTooShortMsg));
		return confirmPasswordIsTooShortMsg.getText();
	}
	
	public String validateMismatchPasswordValidation() {
		wait.until(ExpectedConditions.visibilityOf(mismatchPasswordValidationMsg));
		return mismatchPasswordValidationMsg.getText();
	}
	
	public String validateExpiredInvitationLinkValidation() {
		wait.until(ExpectedConditions.visibilityOf(expiredInvitationLink));
		return expiredInvitationLink.getText();
	}
	
	@FindBy(xpath = "(//td[@data-cb-invoice='Amount'])[1]")
	WebElement customerPortallatestPrice;
	
	public int customerPortalpriceEntrySize() {
		int value = driver.findElements(By.xpath("//td[@data-cb-invoice='Amount']")).size();
		return value;
	}
	
	public void closeAddCreditPopup() {
		wait.until(ExpectedConditions.visibilityOf(closeAddCreditPopup));
		closeAddCreditPopup.click();
	}
	
	public void clickOnLogout() {
		wait.until(ExpectedConditions.visibilityOf(logout));
		logout.click();
	}
	
	// -------------------------start gamil login logic ---------------------

	public void validateMagicLickExpired() {
		gloginGmail();
		openMail();
		gClickOnAcceptInvitationlink();
		// ggetTitle();

	}
	
	public void ValidateInvitatonLink() {
		// gloginGmail();
		openMail();
		gClickOnRecentAcceptInvitationlink();
		ggetTitle();
	}

	public void ValidateVerifyButtonOnGmail() {
		// gloginGmail();
		openMail();
		gClickOnRecentAcceptInvitationlink();
		ggetTitle();
	}
	
	public void ValidateSecondLastInvitationButton() {
		// gloginGmail();
		openMail();
		gClickOnSecondLastRecentAcceptInvitationlink();
		ggetTitle();
	}

	public void gloginGmail() {
		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		// driver.get("https://gmail.com");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		gemail.sendKeys("jayadip@callhippo.com");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		gemailNext.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		gpassword.sendKeys("Jaydeep@8511");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		gpasswordNext.click();
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
		}
		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));
	}

	public void gClickOnRecentAcceptInvitationlink() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]"))));
		int size = driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).size();
		System.out.println(size);

		if (size < 2) {
			driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]")).click();
		} else {

			driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).get(size - 1).click();

		}
	}
	
	public void gClickOnSecondLastRecentAcceptInvitationlink() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]"))));
		int size = driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).size();
		System.out.println(size);

		if (size < 2) {
			driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]")).click();
		} else {

			driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).get(size - 2).click();

		}
	}

	public void gClickOnAcceptInvitationlink() {
		// magicLogin.click();
		// driver.findElement(By.xpath("//a[contains(.,'Magic login')]")).click();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		
		wait.until(
				ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]"))));
		int size = driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).size();
		System.out.println(size);

		if (size > 0) {
			driver.findElement(By.xpath("//a[contains(.,'Accept Invitation')]")).click();
		} else {

			driver.findElements(By.xpath("//a[contains(.,'Accept Invitation')]")).get(size - 1).click();

		}
	}

	public void ggetTitle() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {

		}
//		String mainWindow = driver.getWindowHandle();
//		System.out.println(mainWindow);
		// It returns no. of windows opened by WebDriver and will return Set of Strings
		Set<String> set = driver.getWindowHandles();
		// Using Iterator to iterate with in windows
		Iterator<String> itr = set.iterator();
		while (itr.hasNext()) {

			String childWindow = itr.next();
			System.out.println(childWindow);
			// Compare whether the main windows is not equal to child window. If not equal,
			// we will close.
//			if (!mainWindow.equals(childWindow)) {
			driver.switchTo().window(childWindow);
			// wait.until(ExpectedConditions.titleIs("Register | Callhippo.com"));
			// System.out.println(driver.switchTo().window(childWindow).getTitle());
			// driver1 =driver.switchTo().window(childWindow);
			// return driver.switchTo().window(childWindow).getTitle();
			// driver.close();
//			}
		}
		// return mainWindow;
	}

	public void openMail() {

		driver.get(
				"https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");

		// driver.get("https://gmail.com");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
		int attempts = 0;
		while (attempts < 2) {
			try {
				try {
					driver.findElements(By.xpath("//span[contains(text(),'invited you to join CallHippo')]")).get(1)
							.click();
				} catch (Exception e) {
					wait.until(ExpectedConditions.stalenessOf(
							driver.findElements(By.xpath("//span[contains(text(),'invited you to join CallHippo')]"))
									.get(1)));
					driver.findElements(By.xpath("//span[contains(text(),'invited you to join CallHippo')]")).get(1)
							.click();
				}

				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}

		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == 'complete'"));

	}

	// ------------ end gmail login logic------------------------------

}
