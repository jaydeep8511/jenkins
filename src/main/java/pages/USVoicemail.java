package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class USVoicemail {
	
	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'×')]")WebElement CloseValidationPopup;
	@FindBy(xpath = "//span[@ng-bind-html='message.content']")WebElement validationMessage;
	@FindBy(xpath = "//div[@id='voicemail']//span[@class='lever cttoggel']")WebElement voicemailToggle;
	@FindBy(xpath = "//label[@for='voiceMailType1']") WebElement textRadioButton;
	@FindBy(xpath = "//label[@for='voiceMailType2']") WebElement musicRadioButton;
	@FindBy(xpath = "//input[@name='voiceMailText']") WebElement messageTextbox;
	@FindBy(xpath = "//div[@id='voicemail']//select[@id='ivrSelection']")WebElement languageDropdown;
	@FindBy(xpath = "//label[@for='womanVoiceMail']")WebElement femaleVoiceRadioButton;
	@FindBy(xpath = "//label[@for='manVoiceMail']") WebElement maleVoiceRadioButton;
	@FindBy(xpath = "//div[@id='voicemail']//button[contains(.,'Save')]")WebElement saveButton;
	@FindBy(xpath = "//div[@id='voicemail']//div[@name='VoiceMailFile']")WebElement changeFileButton;
	@FindBy(xpath = "//div[@id='voicemail']//span[@class='help-block fileValidationColor']")WebElement validationText;
	@FindBy(xpath = "//span[contains(.,'Voice mail is required')]")WebElement requirefieldValidation;
	
	@FindBy(xpath = "//button[@class='upgraderecordbtn'][contains(.,'Select Plan')]")WebElement selectPlanButton;
	@FindBy(xpath = "//a[@scroll-on-click='yourplan']") WebElement yourPlan;
	
	public USVoicemail(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));
	}
	
	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}
	
	public void closeValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(CloseValidationPopup));
		CloseValidationPopup.click();
	}
	
	public void clickOnVoicemailToggle() {
		wait.until(ExpectedConditions.visibilityOf(voicemailToggle));
		voicemailToggle.click();
	}
	
	public void clickOnTextRadiobutton() {
		wait.until(ExpectedConditions.visibilityOf(textRadioButton));
		textRadioButton.click();
	}
	
	public void clickOnMusicRadioButton() {
		wait.until(ExpectedConditions.visibilityOf(musicRadioButton));
		musicRadioButton.click();
	}
	
	public void clearMessageTextbox() {
		wait.until(ExpectedConditions.visibilityOf(messageTextbox));
		messageTextbox.clear();
	}
	
	public void enterMessageInTextbox(String message) {
		wait.until(ExpectedConditions.visibilityOf(messageTextbox));
		messageTextbox.sendKeys(message);
	}
	
	public void selectLanguageFromDropdown(String visibleText) {
		wait.until(ExpectedConditions.visibilityOf(languageDropdown));
		Select lan = new Select(languageDropdown);
		lan.selectByVisibleText(visibleText);
	}
	
	public void clickOnFemaleVoiceRadioButton() {
		wait.until(ExpectedConditions.visibilityOf(femaleVoiceRadioButton));
		femaleVoiceRadioButton.click();
	}
	
	public void clickOnMaleVoiceRadioButton() {
		wait.until(ExpectedConditions.visibilityOf(maleVoiceRadioButton));
		maleVoiceRadioButton.click();
	}
	
	public void clickOnSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton));
		saveButton.click();
	}
	
	public void clickOnchangeFileButton() {
		wait.until(ExpectedConditions.visibilityOf(changeFileButton));
		changeFileButton.click();
	}
	
	public String ValidationText() {
		wait.until(ExpectedConditions.visibilityOf(validationText));
		return validationText.getText();
	}

	public String requiredFieldValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(requirefieldValidation));
		return requirefieldValidation.getText();
	}
	
	public boolean selectPlanPopupIsDisplay() {
		wait.until(ExpectedConditions.visibilityOf(selectPlanButton));
		return selectPlanButton.isDisplayed();
	}
	
	public void clickOnSelectPlanButton() {
		wait.until(ExpectedConditions.visibilityOf(selectPlanButton));
		selectPlanButton.click();
	}
	
	public void clickOnYourPlanLink() {
		wait.until(ExpectedConditions.visibilityOf(yourPlan));
		yourPlan.click();
	}
}
