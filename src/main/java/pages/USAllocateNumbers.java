package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class USAllocateNumbers {
	
	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@FindBy(xpath = "//button[@type='button'][contains(.,'×')]")
	WebElement CloseValidationPopup;
	@FindBy(xpath = "//span[@ng-bind-html='message.content']")
	WebElement validationMessage;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'A number should be allocated to atleast one user.')]") WebElement allocateErrorMsg;
	@FindBy(xpath = "//button[text()='Yes']") WebElement yesButton;
	@FindBy(xpath = "//button[text()='No']") WebElement noButton;
	
	
	public USAllocateNumbers(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));
	}

	public String validationMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}

	public void closeValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(CloseValidationPopup));
		CloseValidationPopup.click();
	}
	
	public void clickOnNumber(String number) {
		String path = "//span[@class='countrylist_numberselect ng-binding'][contains(.,'"+number+"')]/parent::div/parent::div";
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		WebElement el = driver.findElement(By.xpath(path));
		el.click();
	}
	
	public void waitForAllocateErrorMsg() {
		wait.until(ExpectedConditions.visibilityOf(allocateErrorMsg));
	}
	
	public void clickOnDefaultNumberCheckBoxOf(String number) {
		String path = "//span[@class='countrylist_numberselect ng-binding'][contains(.,'"+number+"')]/parent::div//following-sibling::a/i";
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(path))));
		WebElement el = driver.findElement(By.xpath(path));
		el.click();
	}
	
	public boolean validateByDefaultNumberCheckboxIsDisplayed(String number) {
		
		try {
			String path = "//span[@class='countrylist_numberselect ng-binding'][contains(.,'"+number+"')]/parent::div//following-sibling::a[@class='ntickdnch ng-hide']";
			driver.findElement(By.xpath(path)).isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}
	
	}
	
	public void clickOnYesButton() {
		wait.until(ExpectedConditions.visibilityOf(yesButton));
		yesButton.click();
	}

	public void clickOnNoButton() {
		wait.until(ExpectedConditions.visibilityOf(noButton));
		noButton.click();
	}

	
}
