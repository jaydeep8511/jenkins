package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DeleteNumber {

	static WebDriver driver;
	WebDriverWait wait;
	Actions action;

	@FindBy(xpath = "(//button[@class='circledelete btn btn-sm btn-danger'])[1]")WebElement deleteIcon;
	@FindBy(xpath="//p[@ng-if='options.message'][contains(.,'Are you sure you want to delete this number ?')]") WebElement deletePopupMessage;
	@FindBy(xpath = "//button[@ng-repeat='button in options.buttons track by button.label'][contains(.,'Yes')]")WebElement yesButton;
	@FindBy(xpath="//button[@ng-repeat='button in options.buttons track by button.label'][contains(.,'No')]")WebElement noButton;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Number deleted successfully')]") WebElement validationMessage;
	
	public DeleteNumber(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}
	
	public boolean validateDeleteIconIsDisplayed() {
		wait.until(ExpectedConditions.visibilityOf(deleteIcon));
		return deleteIcon.isDisplayed();
	}
	
	public boolean validateDeleteIconIsDisplayed1() {
		wait.until(ExpectedConditions.invisibilityOf(deleteIcon));
		return deleteIcon.isDisplayed();
	}
	public void clickOnDeleteIcon() {
		wait.until(ExpectedConditions.visibilityOf(deleteIcon));
		deleteIcon.click();
	}
	
	public String validateDeletePopupText() {
		wait.until(ExpectedConditions.visibilityOf(deletePopupMessage));
		return deletePopupMessage.getText();
	}
	
	public void clickOnYesButtonOfDeletePopup() {
		wait.until(ExpectedConditions.visibilityOf(yesButton));
		yesButton.click();
	}
	
	public void clickOnNeButtonOfDeletePopup() {
		wait.until(ExpectedConditions.visibilityOf(noButton));
		noButton.click();
	}
	
	public String validateDeleteNumberValidationNumber() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}
	
}
