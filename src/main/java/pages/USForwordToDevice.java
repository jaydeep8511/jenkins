package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class USForwordToDevice {
	
	static WebDriver driver;
	WebDriverWait wait;
	Actions action;
	
	@FindBy(xpath = "//button[@type='button'][contains(.,'×')]")WebElement CloseValidationPopup;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'User updated successfully')]")WebElement waitForValidationMessage;
	@FindBy(xpath = "//span[@ng-bind-html='message.content'][contains(.,'Please Enter Number With + and CountryCode.')]")WebElement waitForErrorValidationMessage;
	@FindBy(xpath = "//span[@ng-bind-html='message.content']")WebElement validationMessage;
	
	@FindBy(xpath = "//div[@id='number']//span[@class='lever cttoggel']")WebElement ftdToggle;
	@FindBy(xpath="//div[@id='number']//i[contains(@class,'fa fa-pencil')]")WebElement editNumberButton;
	@FindBy(xpath = "//input[@ng-model='device.number']") WebElement numberTextbox;
	@FindBy(xpath = "//div[@class='col-md-3 ng-scope']//i[@class='fa fa-floppy-o']")WebElement saveButton;
	@FindBy(xpath = "//a[@class='editpencil circlest btn btn-sm btn-info ng-scope']")WebElement addButton;
	@FindBy(xpath = "//input[@name='mobile_1']") WebElement numberTextbox2;
	@FindBy(xpath = "(//i[@class='fa fa-trash'])[1]")WebElement deleteButton;
	@FindBy(xpath = "(//i[@class='fa fa-trash'])[2]")WebElement deleteButton2;
	
	

	
	public USForwordToDevice(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 20);
		action = new Actions(this.driver);
	}

	public void wait_angular5andjs() {
		JavascriptExecutor js = (JavascriptExecutor) this.driver;
		wait.until(ExpectedConditions.jsReturnsValue(
				"return angular.element(document).injector().get('$http').pendingRequests.length === 0"));
		wait.until(ExpectedConditions.jsReturnsValue("return jQuery.active==0"));

	}
	
	public void waitForValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(waitForValidationMessage));
	}
	
	public void waitForErrorValidationMessage() {
		wait.until(ExpectedConditions.visibilityOf(waitForErrorValidationMessage));
	}
	
	public String ValidateUserUpdateMessage() {
		wait.until(ExpectedConditions.visibilityOf(validationMessage));
		return validationMessage.getText();
	}
	
	public void clearNumberTextbox() {
		wait.until(ExpectedConditions.visibilityOf(numberTextbox));
		numberTextbox.clear();
	}
	
	public void enterNumberInTextbox(String number) {
		wait.until(ExpectedConditions.visibilityOf(numberTextbox));
		numberTextbox.sendKeys(number);
	}
	
	public void enterNumberInTextbox2(String number) {
		wait.until(ExpectedConditions.visibilityOf(numberTextbox2));
		numberTextbox2.sendKeys(number);
	}
	
	public void closeValidationMessagePopup() {
		wait.until(ExpectedConditions.visibilityOf(CloseValidationPopup));
		CloseValidationPopup.click();
	}
	
	public void clickOnForwordToDeviceToggle() {
		wait.until(ExpectedConditions.visibilityOf(ftdToggle));
		ftdToggle.click();
	}
	
	public void ClickOnEditNumberButton() {
		wait.until(ExpectedConditions.visibilityOf(editNumberButton));
		editNumberButton.click();
	}
	
	public void ClickOnSaveButton() {
		wait.until(ExpectedConditions.visibilityOf(saveButton));
		saveButton.click();
	}
	
	public void ClickOnAddButton() {
		wait.until(ExpectedConditions.visibilityOf(addButton));
		addButton.click();
	}
	
	public void clickOn1stDeletenumber() {
		wait.until(ExpectedConditions.visibilityOf(deleteButton));
		deleteButton.click();
	}
	
	public void clickOn2ndDeletenumber() {
		wait.until(ExpectedConditions.visibilityOf(deleteButton2));
		deleteButton2.click();
	}
}
